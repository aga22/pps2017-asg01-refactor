package u02lab.code;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private static final int START = 5;
    private static final int STOP = 10;
    private GeneratorFactory factory = new GeneratorFactoryImpl();
    private SequenceGenerator generator = factory.createRangeGenerator(START, STOP);

    @Test
    public void next() {
        for(int i = START; i <= STOP;i++){
            assertTrue(this.generator.next().isPresent());
        }
        assertFalse(this.generator.next().isPresent());
    }

    @Test
    public void reset() {
        this.generator.next();
        this.generator.next();
        this.generator.reset();
        for(int i = START; i <= STOP;i++){
            assertTrue(this.generator.next().isPresent());
        }
        this.generator.reset();
        for(int i = START; i <= STOP;i++){
            assertTrue(this.generator.next().isPresent());
        }
    }

    @Test
    public void isOver() {
        this.generator.next();
        this.generator.next();
        assertFalse(this.generator.isOver());
        this.generator.reset();
        for(int i = START; i <= STOP;i++){
            this.generator.next();
        }
        assertTrue(this.generator.isOver());
    }

    @Test
    public void allRemaining() {
        this.generator.next();
        this.generator.next();
        List<Integer> list = new LinkedList<>();
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        assertEquals(list, this.generator.allRemaining());
    }
}