package u02lab.code;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class RandomGeneratorTest {

    private static final int NUMBER = 5;
    private GeneratorFactory factory = new GeneratorFactoryImpl();
    private SequenceGenerator generator = factory.createRandomGenerator(NUMBER);

    @Test
    public void next() {
        this.generator.next();
        this.generator.next();
        assertTrue(this.generator.next().isPresent());
        this.generator.next();
        this.generator.next();
        assertFalse(this.generator.next().isPresent());
    }

    @Test
    public void reset() {
        this.generator.next();
        this.generator.reset();
        for(int i = 0; i < NUMBER;i++){
            assertTrue(this.generator.next().isPresent());
        }
    }

    @Test
    public void isOver() {
        this.generator.next();
        this.generator.next();
        this.generator.next();
        assertFalse(this.generator.isOver());
        this.generator.next();
        this.generator.next();
        assertTrue(this.generator.isOver());
    }

    @Test
    public void allRemaining() {
        this.generator.next();
        this.generator.next();
        assertEquals(3, this.generator.allRemaining().size());
    }
}