package u02lab.code;

public class GeneratorFactoryImpl implements GeneratorFactory {
    @Override
    public SequenceGenerator createRandomGenerator(int number) {
        return new RandomGenerator(number);
    }

    @Override
    public SequenceGenerator createRangeGenerator(int start, int stop) {
        return new RangeGenerator(start, stop);
    }
}
