package u02lab.code;

public interface GeneratorFactory {

    SequenceGenerator createRandomGenerator(final int number);

    SequenceGenerator createRangeGenerator(final int start, final int stop);
}
