package u02lab.code;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private List<Integer> numbers;
    private int value;
    private int index;

    RandomGenerator(int n){
        this.value = n;
        this.numbers = new LinkedList<>();
        this.index = 0;
        for (int i = 0; i < this.value; i++){
            if (Math.random() < 0.5) {
                this.numbers.add(0);
            } else {
                this.numbers.add(1);
            }
        }
    }

    @Override
    public Optional<Integer> next() {
        if (this.index < this.value) {
            int nextNumber = this.numbers.get(this.index);
            this.index++;
            return Optional.of(nextNumber);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.index = 0;
    }

    @Override
    public boolean isOver() {
        return this.index >= this.value;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remaining = new LinkedList<>();
        for (int i = this.index; i < this.value; i++) {
            remaining.add(this.numbers.get(i));
        }
        return remaining;
    }
}
