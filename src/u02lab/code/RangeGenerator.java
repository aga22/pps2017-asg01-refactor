package u02lab.code;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private List<Integer> numbers;
    private int index;
    private int start;
    private int stop;

    RangeGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
        this.index = 0;
        this.numbers = new LinkedList<>();
        for(int i = this.start; i <= this.stop;i++){
            this.numbers.add(i);
        }
    }

    @Override
    public Optional<Integer> next() {
        if (this.index <= (this.stop - this.start)) {
            int nextNumber = this.numbers.get(this.index);
            this.index++;
            return Optional.of(nextNumber);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.index = 0;
    }

    @Override
    public boolean isOver() {
        return this.index > (this.stop - this.start);
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remaining = new LinkedList<>();
        for (int i = this.index; i <= (this.stop - this.start); i++) {
            remaining.add(this.numbers.get(i));
        }
        return remaining;
    }
}
