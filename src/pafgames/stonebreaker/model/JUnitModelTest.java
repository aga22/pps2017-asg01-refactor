package pafgames.stonebreaker.model;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class JUnitModelTest {

    private final GameBoard gb = new GameBoardImpl();
    private final GameBoard gb2 = new GameBoardImpl();

    /* Questo metodo stampa la Board come una matrice di lettere corrispondenti a:
     * -E: Empty
     * -B: Block (blocco che non pu� essere usato per fare combo)
     * -L'iniziale delle gemme elencate nella enum nella classe Gem
     */
    private void printBoard() {
        String readableboard = "";
        for (int i = 0; i < this.gb.getRows(); i++) {
            for (int j = 0; j < this.gb.getColumns(); j++) {
                if (this.gb.getBoard().get(i).get(j).equals(Optional.empty())) {
                    readableboard = readableboard.concat("E ");
                } else if (this.gb.getBoard().get(i).get(j).isPresent() && this.gb.getBoard().get(i).get(j).get() instanceof Gem) {
                    readableboard = readableboard.concat(
                            Character.toString(
                                    ((Gem) this.gb.getBoard().get(i).get(j).get()).getColour().name().charAt(0)) + " ");
                } else if (this.gb.getBoard().get(i).get(j).get() instanceof Block) {
                    readableboard = readableboard.concat("B ");
                }
            }
            readableboard = readableboard.concat("\n");
        }
        System.out.println(readableboard);
    }

    @org.junit.Test
    public void largeScaleTest() {
        // [1] Verifico che la matrice sia vuota
        System.out.println("[1]");
        printBoard();
        for (int i = 0; i < this.gb.getRows(); i++) {
            for (int j = 0; j < this.gb.getColumns(); j++) {
                assertEquals(this.gb.getBoard().get(i).get(j), Optional.empty());
            }
        }
        // [2] Setto la difficolt� ad EASY = 0, popolo la matrice e verifico assenza di Optional.empty()
        System.out.println("[2]");
        this.gb.getPlayer().setDifficulty(0);
        this.gb.repopulate();
        printBoard();
        for (int i = 0; i < this.gb.getRows(); i++) {
            for (int j = 0; j < this.gb.getColumns(); j++) {
                assertNotEquals(this.gb.getBoard().get(i).get(j), Optional.empty());
            }
        }
        // [3] Verifico l'assenza di gemme PURPLE e WHITE (P e W) perch� la difficolt� � EASY
        System.out.println("[3]");
        printBoard();
        for (int i = 0; i < this.gb.getRows(); i++) {
            for (int j = 0; j < this.gb.getColumns(); j++) {
                if (this.gb.getBoard().get(i).get(j).isPresent() && this.gb.getBoard().get(i).get(j).get() instanceof Gem) {
                    assertNotEquals(((Gem) this.gb.getBoard().get(i).get(j).get()).getColour(), Gem.Type.PURPLE);
                    assertNotEquals(((Gem) this.gb.getBoard().get(i).get(j).get()).getColour(), Gem.Type.WHITE);
                }
            }
        }
        // [4] Svuoto la matrice e verifico che sia composta da sole Optional.empty (E)
        System.out.println("[4]");
        for (int i = 0; i < this.gb.getRows(); i++) {
            for (int j = 0; j < this.gb.getColumns(); j++) {
                if (this.gb.getBoard().get(i).get(j).isPresent()) {
                    this.gb.getBoard().get(i).get(j).get().deleteMe();
                }
            }
        }
        this.gb.destroyMarked();
        printBoard();
        for (int i = 0; i < this.gb.getRows(); i++) {
            for (int j = 0; j < this.gb.getColumns(); j++) {
                assertEquals(this.gb.getBoard().get(i).get(j), Optional.empty());
            }
        }
        // [5] Setto ora la difficolt� a MEDIUM (1) e ripopolo la matrice, verificando l'assenza delle sole WHITE (W)
        System.out.println("[5]");
        this.gb.getPlayer().setDifficulty(1);
        this.gb.repopulate();
        printBoard();
        for (int i = 0; i < this.gb.getRows(); i++) {
            for (int j = 0; j < this.gb.getColumns(); j++) {
                if (this.gb.getBoard().get(i).get(j).isPresent() && this.gb.getBoard().get(i).get(j).get() instanceof Gem) {
                    assertNotEquals(((Gem) this.gb.getBoard().get(i).get(j).get()).getColour(), Gem.Type.WHITE);
                }
            }
        }
        // [6] Elimino l'ultima riga...
        System.out.println("[6]");
        for (int j = 0; j < this.gb.getColumns(); j++) {
            if (this.gb.getBoard().get(this.gb.getRows() - 1).get(j).isPresent()) {
                this.gb.getBoard().get(this.gb.getRows() - 1).get(j).get().deleteMe();
            }
        }
        this.gb.destroyMarked();
        printBoard();
        // [7] ...faccio cadere le gemme sovrastanti...
        System.out.println("[7]");
        this.gb.fallDown();
        printBoard();
        // [8] ...e ripopolo le celle vuote
        System.out.println("[8]");
        this.gb.repopulate();
        printBoard();
    }

    @org.junit.Test
    public void smallScaleTest() {
        // Setto la difficolt� a EASY e ripopolo la matrice
        this.gb2.getPlayer().setDifficulty(0);
        this.gb2.repopulate();
        // Testo la correttezza del metodo isNeighBour della classe GameBoardImpl
        final List<List<Optional<Stone>>> l = this.gb2.getBoard();
        if (l.get(0).get(0).isPresent() && l.get(0).get(1).isPresent() &&
                l.get(1).get(0).isPresent() && l.get(3).get(5).isPresent() &&
                l.get(1).get(1).isPresent() && this.gb2.getBoard().get(0).get(0).isPresent() &&
                this.gb2.getBoard().get(3).get(3).isPresent()) {
            assertTrue(this.gb2.isNeighbour(l.get(0).get(0).get(), l.get(0).get(1).get()));  //(0,0) e (0,1)
            assertTrue(this.gb2.isNeighbour(l.get(0).get(0).get(), l.get(1).get(0).get()));  //(0,0) e (1,0)
            assertFalse(this.gb2.isNeighbour(l.get(0).get(0).get(), l.get(1).get(1).get())); //(0,0) e (1,1)
            assertFalse(this.gb2.isNeighbour(l.get(0).get(0).get(), l.get(3).get(5).get())); //(0,0) e (3,5)
            // Testo la correttezza del metodo swap della classe GameBoardImpl
            Stone s1 = this.gb2.getBoard().get(0).get(0).get();
            Stone s2 = this.gb2.getBoard().get(3).get(3).get();
            this.gb2.swap(s1, s2);
            assertEquals(s1, this.gb2.getBoard().get(3).get(3).get());
            assertEquals(s2, this.gb2.getBoard().get(0).get(0).get());
            // Testo la correttezza dei metodi get[Right/Left/Up/Down]Stone della classe GameBoardImpl
            Optional<Stone> os = this.gb2.getBoard().get(1).get(1);
            assertEquals(this.gb2.getDownStone(this.gb2.getBoard().get(0).get(1)), os);
            assertEquals(this.gb2.getUpStone(this.gb2.getBoard().get(2).get(1)), os);
            assertEquals(this.gb2.getRightStone(this.gb2.getBoard().get(1).get(0)), os);
            assertEquals(this.gb2.getLeftStone(this.gb2.getBoard().get(1).get(2)), os);
        }
    }

}
