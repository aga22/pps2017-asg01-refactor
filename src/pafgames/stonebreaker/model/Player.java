package pafgames.stonebreaker.model;

import java.util.List;

/**
 * An interface for the difficulty of the game and the Gems selected by the player in game
 */
public interface Player {

    /**
     * @return the current difficulty of the game
     */
    int getDifficulty();

    /**
     * Sets the difficulty of the game
     *
     * @param d, the difficulty
     */
    void setDifficulty(int d);

    /**
     * Add the gem selected by the player during the game to a list
     *
     * @param s, the selected gem
     */
    void addSelectedStone(Gem s);

    /**
     * @return the list of selected gems
     */
    List<Gem> getSelectedStones();
}
