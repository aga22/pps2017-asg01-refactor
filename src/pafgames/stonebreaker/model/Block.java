package pafgames.stonebreaker.model;

/**
 * This class implements a Block. A Block is a non swappable item on the board
 * that will be deleted if it reaches the bottom of the game matrix or if the
 * player uses the powerup. It won't be deleted even if aligned with other Blocks.
 */
public class Block extends StoneImpl {

    public Block(final int posx, final int posy) {
        super(posx, posy);
    }

    public String toString() {
        return super.toString() + " / SONO UN BLOCCO\n";
    }
}
