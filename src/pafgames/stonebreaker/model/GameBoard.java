package pafgames.stonebreaker.model;

import java.util.List;
import java.util.Optional;

/**
 * An interface for the the board of the game
 */
public interface GameBoard {

    /**
     * @return the number of rows of the board
     */
    int getRows();

    /**
     * @return the number of columns of the board
     */
    int getColumns();

    /**
     * Fills the board with Stones, putting a new Stone wherever
     * it finds a slot containing an Optional.empty()
     */
    void repopulate();

    /**
     * @return the game board
     */
    List<List<Optional<Stone>>> getBoard();

    /**
     * @return the game board, but as a single list of Stones.
     */
    List<Optional<Stone>> getBoardAsList();

    /**
     * @return an object of the Player class
     */
    Player getPlayer();

    /**
     * @param s, a Stone on the board
     * @return the Stone above the given one or Optional.empty()
     * if the given one was in the first row
     */
    Optional<Stone> getUpStone(Optional<Stone> s);

    /**
     * @param s, a Stone on the board
     * @return the Stone below the given one or Optional.empty()
     * if the given one was in the last row
     */
    Optional<Stone> getDownStone(Optional<Stone> s);

    /**
     * @param s, a Stone on the board
     * @return the Stone on the left side of the given one or Optional.empty()
     * if the given one was in the first column
     */
    Optional<Stone> getLeftStone(Optional<Stone> s);

    /**
     * @param s, a Stone on the board
     * @return the Stone on the right side of the given one or Optional.empty()
     * if the given one was in the last column
     */
    Optional<Stone> getRightStone(Optional<Stone> s);

    /**
     * @param s1, a Stone on the board
     * @param s2, another Stone on the board
     * @return true if the two given Stones were near each other
     * (one tile distant either horizontally or vertically)
     */
    boolean isNeighbour(Stone s1, Stone s2);

    /**
     * Swaps the two given Stones on the board
     *
     * @param s1, a Stone on the board
     * @param s2, another Stone on the board
     */
    void swap(Stone s1, Stone s2);

    /**
     * Destroy every Stone on the board that were previously
     * marked for deleting
     */
    void destroyMarked();

    /**
     * Makes every Stone that has no other Stone beneath fall down
     * until the only empty tiles on the board are in the upper rows.
     */
    void fallDown();
}
