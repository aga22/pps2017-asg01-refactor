package pafgames.stonebreaker.model;

/**
 * This class implements Stone
 */
public class StoneImpl implements Stone {

    private int posx;
    private int posy;
    private boolean markfordelete = false;

    public StoneImpl(int x, int y) {
        this.posx = x;
        this.posy = y;
    }

    @Override
    public int getX() {
        return posx;
    }

    @Override
    public void setX(int x) {
        this.posx = x;
    }

    @Override
    public int getY() {
        return posy;
    }

    @Override
    public void setY(int y) {
        this.posy = y;
    }

    @Override
    public void deleteMe() {
        this.markfordelete = true;
    }

    @Override
    public boolean isBeingDeleted() {
        return this.markfordelete;
    }

    public String toString() {
        return "Posizione:(" + this.posx + ";" + this.posy + ")";
    }
}
