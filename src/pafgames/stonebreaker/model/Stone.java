package pafgames.stonebreaker.model;

/**
 * An interface for a generic piece on the board
 */
public interface Stone {

    /**
     * @return the x coordinate of the stone on the board
     */
    int getX();

    /**
     * Setter for the x coordinate of the stone
     *
     * @param x, the new coordinate
     */
    void setX(int x);

    /**
     * @return the y coordinate of the stone on the board
     */
    int getY();

    /**
     * Setter for the y coordinate of the stone
     *
     * @param y, the new coordinate
     */
    void setY(int y);

    /**
     * Sets a boolean flag to true, saying that this stone will be deleted
     */
    void deleteMe();

    /**
     * @return true if this stone will be deleted
     */
    boolean isBeingDeleted();
}
