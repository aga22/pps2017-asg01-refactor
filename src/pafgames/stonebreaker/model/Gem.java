package pafgames.stonebreaker.model;

/**
 * This class implements a Gem. A gem has one of the colors defined in the enumeration Type
 * (randomly chosen), can be swapped by the player and can be deleted if aligned with two
 * or more gems of the same color or if the player uses the powerup. Deleting gems will give
 * the player points that will rise his score.
 */
public class Gem extends StoneImpl {

    /**
     * This is the color of the gem
     */
    private final Type colour;

    /**
     * This constructor will give a random color to the gem, depending on the
     * difficulty of the game chosen by the player
     */
    public Gem(int posx, int posy, int diff) {
        super(posx, posy);
        int BASETYPES = 4;
        this.colour = Type.values()[(int) (Math.random() * (diff + BASETYPES))];
    }

    /**
     * @return the color of the gem
     */
    public Type getColour() {
        return this.colour;
    }

    public String toString() {
        return super.toString() + " / Colore:" + this.colour + "\n";
    }

    /**
     * The enumeration containing the different colors of the gems.
     */
    public enum Type {
        RED, CYAN, YELLOW, GREEN, PURPLE, WHITE
    }
}
