package pafgames.stonebreaker.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * This class implements GameBoard
 */
public class GameBoardImpl implements GameBoard {

    /**
     * This indicates the number of rows of the board
     */
    private final int BOARD_ROWS = 7;

    /**
     * This indicates the number of columns of the board
     */
    private final int BOARD_COLUMNS = 7;

    /**
     * This is the board containing either Stones or Optional.empty()
     */
    private final List<List<Optional<Stone>>> board = new LinkedList<>();

    /**
     * This is an object of the Player class
     */
    private final Player play = new PlayerImpl();

    /**
     * This constructor creates the game board and sets every tile as Optional.empty()
     */
    public GameBoardImpl() {
        for (int i = 0; i < this.BOARD_ROWS; i++) {
            List<Optional<Stone>> row = new LinkedList<>();
            this.board.add(i, row);
            for (int j = 0; j < this.BOARD_COLUMNS; j++) {
                final Optional<Stone> opt = Optional.empty();
                row.add(j, opt);
            }
        }
    }

    @Override
    public int getRows() {
        return this.BOARD_ROWS;
    }

    @Override
    public int getColumns() {
        return this.BOARD_COLUMNS;
    }

    @Override
    public void repopulate() {
        for (int i = 0; i < this.BOARD_ROWS; i++) {
            for (int j = 0; j < this.BOARD_COLUMNS; j++) {
                if (this.board.get(i).get(j).equals(Optional.empty())) {
                    final double rnd = Math.random();
                    double BLOCK_CHANCE = 0.1;
                    if (rnd <= BLOCK_CHANCE) {
                        this.board.get(i).set(j, Optional.of(new Block(i, j)));
                    } else {
                        this.board.get(i).set(j, Optional.of(new Gem(i, j, play.getDifficulty())));
                    }
                }
            }
        }
    }

    @Override
    public List<List<Optional<Stone>>> getBoard() {
        return this.board;
    }

    @Override
    public List<Optional<Stone>> getBoardAsList() {
        final List<Optional<Stone>> singlelist = new LinkedList<>();
        for (int i = 0; i < this.BOARD_ROWS; i++) {
            for (int j = 0; j < this.BOARD_COLUMNS; j++) {
                singlelist.add(this.board.get(i).get(j));
            }
        }
        return singlelist;
    }

    public Player getPlayer() {
        return this.play;
    }

    @Override
    public Optional<Stone> getUpStone(Optional<Stone> s) {
        if (!s.equals(Optional.empty()) && s.isPresent() && s.get().getX() > 0) {
            return this.board.get(s.get().getX() - 1).get(s.get().getY());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Stone> getDownStone(Optional<Stone> s) {
        if (!s.equals(Optional.empty()) && s.isPresent() && s.get().getX() < (this.BOARD_ROWS - 1)) {
            return this.board.get(s.get().getX() + 1).get(s.get().getY());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Stone> getLeftStone(Optional<Stone> s) {
        if (!s.equals(Optional.empty()) && s.isPresent() && s.get().getY() > 0) {
            return this.board.get(s.get().getX()).get(s.get().getY() - 1);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Stone> getRightStone(Optional<Stone> s) {
        if (!s.equals(Optional.empty()) && s.isPresent() && s.get().getY() < this.BOARD_COLUMNS - 1) {
            return this.board.get(s.get().getX()).get(s.get().getY() + 1);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean isNeighbour(Stone s1, Stone s2) {
        final int distX = Math.abs(s1.getX() - s2.getX());
        final int distY = Math.abs(s1.getY() - s2.getY());
        final int distTot = distX + distY;
        return distTot == 1;
    }

    @Override
    public void swap(Stone s1, Stone s2) {
        this.board.get(s1.getX()).set(s1.getY(), Optional.of(s2));
        this.board.get(s2.getX()).set(s2.getY(), Optional.of(s1));
        final int boxX = s1.getX();
        final int boxY = s1.getY();
        updateCoordinates(s1, s2.getX(), s2.getY());
        updateCoordinates(s2, boxX, boxY);
    }

    private void updateCoordinates(Stone s, int x, int y) {
        s.setX(x);
        s.setY(y);
    }

    @Override
    public void destroyMarked() {
        for (int i = 0; i < this.BOARD_ROWS; i++) {
            for (int j = 0; j < this.BOARD_COLUMNS; j++) {
                if (this.board.get(i).get(j).isPresent() && this.board.get(i).get(j).get().isBeingDeleted()) {
                    this.board.get(i).set(j, Optional.empty());
                }
            }
        }
    }

    @Override
    public void fallDown() {
        boolean hasfinished;
        do {
            hasfinished = true;
            for (int i = 0; i < this.BOARD_ROWS - 1; i++) {
                for (int j = 0; j < this.BOARD_COLUMNS; j++) {
                    if (!this.board.get(i).get(j).equals(Optional.empty())) {
                        if (getDownStone(this.board.get(i).get(j)).equals(Optional.empty())) {
                            this.board.get(i + 1).set(j, this.board.get(i).get(j));
                            if (this.board.get(i + 1).get(j).isPresent()) {
                                updateCoordinates(this.board.get(i + 1).get(j).get(), i + 1, j);
                                this.board.get(i).set(j, Optional.empty());
                                hasfinished = false;
                            }
                        }
                    }

                }
            }
        } while (!hasfinished);
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < this.BOARD_ROWS; i++) {
            for (int j = 0; j < this.BOARD_COLUMNS; j++) {
                if (this.board.get(i).get(j).equals(Optional.empty())) {
                    s.append("Posizione: (").append(i).append(";").append(j).append(") / SONO EMPTY\n");
                } else if (this.board.get(i).get(j).isPresent() && this.board.get(i).get(j).get() instanceof Gem) {
                    s.append(this.board.get(i).get(j).get().toString());
                } else if (this.board.get(i).get(j).get() instanceof Block) {
                    s.append(this.board.get(i).get(j).get().toString());
                }
            }
            s.append("\n");
        }
        return s.toString();
    }
}
