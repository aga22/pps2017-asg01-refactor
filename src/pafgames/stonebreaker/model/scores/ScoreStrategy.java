package pafgames.stonebreaker.model.scores;

import pafgames.stonebreaker.model.Stone;

public interface ScoreStrategy {

    /**
     * Update current score.
     *
     * @param occurence: the times that a stone similar to the previous occur.
     */
    void updateScore(final Stone gem, final int occurence);

    /**
     * @return the score in a given moment.
     */
    int getScore();
}
