package pafgames.stonebreaker.model.scores;

import pafgames.stonebreaker.model.Stone;

public class SimpleScoreStrategy implements ScoreStrategy {
    /**
     * Constant for number of similar stone.
     */
    private static final int LIMITOFOCCUR = 2;
    /**
     * Field used as reference for score.
     */
    private int totalScore;

    public SimpleScoreStrategy() {
        this.totalScore = 0;
    }

    @Override
    public void updateScore(final Stone gem, int occurence) {
        if (occurence >= LIMITOFOCCUR) {
            this.totalScore = this.totalScore + (100 * occurence);
        }
    }

    @Override
    public int getScore() {
        return this.totalScore;
    }
}
