package pafgames.stonebreaker.model.scores;

import pafgames.stonebreaker.model.Gem;
import pafgames.stonebreaker.model.Stone;

public class AdvScoreStrategy implements ScoreStrategy {

    private int totalScore;

    public AdvScoreStrategy() {
        this.totalScore = 0;
    }

    @Override
    public void updateScore(final Stone stone, int occurence) {
        if (stone instanceof Gem) {
            if (occurence == Points.LIMITFORPOWERUP.getNum()) {
                this.totalScore += Points.POINTSFORPOWERUP.getNum();
            } else {
                switch (((Gem) stone).getColour()) {
                    case RED:
                        totalScore += Points.POINTSFORRED.getNum();
                        break;
                    case CYAN:
                        totalScore += Points.POINTSFORCYAN.getNum();
                        break;
                    case YELLOW:
                        totalScore += Points.POINTSFORYELLOW.getNum();
                        break;
                    case GREEN:
                        totalScore += Points.POINTSFORGREEN.getNum();
                        break;
                    case PURPLE:
                        totalScore += Points.POINTSFORPURPLE.getNum();
                        break;
                    case WHITE:
                        totalScore += Points.POINTSFORWHITE.getNum();
                        break;
                }
            }
        }
    }

    @Override
    public int getScore() {
        return this.totalScore;
    }
}
