package pafgames.stonebreaker.model.scores;

public enum Points {

    POINTSFORRED(100), POINTSFORPOWERUP(1000), POINTSFORCYAN(200),
    POINTSFORYELLOW(300), POINTSFORGREEN(400), POINTSFORPURPLE(500),
    POINTSFORWHITE(600), LIMITFORPOWERUP(9);

    private int num;

    Points(int i) {
        this.num = i;
    }

    int getNum() {
        return this.num;
    }
}
