package pafgames.stonebreaker.model.scores;

import pafgames.stonebreaker.model.Stone;

/**
 * Class that implements Score interface.
 */
public class ScoreImpl {

    private ScoreStrategy strategy;

    /**
     * Constructor for the class.
     */
    public ScoreImpl(final ScoreStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Updates total score.
     */
    public void updateScore(final Stone gem, final int occurence) {
        this.strategy.updateScore(gem, occurence);
    }

    /**
     * Getter for total score.
     */
    public int getScore() {
        return this.strategy.getScore();
    }
}