package pafgames.stonebreaker.model.mode;

/**
 * Interface for a counter.
 */
public interface Counter {

    /**
     * @return counter value.
     */
    int getVal();

    /**
     * Decrements counter value.
     */
    void dec();

    /**
     * @return true if counter = 0, else false.
     */
    boolean isEnded();

}
