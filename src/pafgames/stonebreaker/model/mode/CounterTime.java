package pafgames.stonebreaker.model.mode;

/**
 * Class that implements Counter and used for Time Agent.
 */
public class CounterTime implements Counter {
    /**
     * Constant used to define limit for time in the game.
     */
    private static final int MAX_TEMP = 60;
    /**
     * Variable used for current value.
     */
    private int val;

    /**
     * Constructor used to initialize val.
     */
    public CounterTime() {
        this.val = MAX_TEMP;
    }

    /**
     * Getter for variable val.
     */
    @Override
    public synchronized int getVal() {
        return this.val;
    }

    /**
     * Decrements val.
     */
    @Override
    public synchronized void dec() {
        this.val--;
    }

    /**
     * Define when counter is finished.
     */
    @Override
    public synchronized boolean isEnded() {
        return this.val == -1;
    }

}
