package pafgames.stonebreaker.model.mode;

/**
 * Class that implements Counter and used for Move Agent.
 */
public class CounterMove implements Counter {
    /**
     * Constant used to define limit for time in the game.
     */
    private static final int NUMBEROFMOVES = 20;
    /**
     * Variable used for current value.
     */
    private int val;

    /**
     * Constructor used to initialize val.
     */
    public CounterMove() {
        this.val = NUMBEROFMOVES;
    }

    /**
     * Getter for variable val.
     */
    @Override
    public synchronized int getVal() {
        return this.val;
    }

    /**
     * Decrements val.
     */
    @Override
    public synchronized void dec() {
        this.val--;
    }

    /**
     * Define when counter is finished.
     */
    @Override
    public synchronized boolean isEnded() {
        return this.val == 0;
    }

}
