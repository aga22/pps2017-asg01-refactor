package pafgames.stonebreaker.model;

import java.util.LinkedList;
import java.util.List;

/**
 * There are 3 difficulties in the game:
 * - EASY = 0
 * - MEDIUM = 1
 * - HARD = 2
 */
public class PlayerImpl implements Player {

    /**
     * This List holds the gems selected by the player, the gems he wants to swap
     */
    private final List<Gem> toSwap = new LinkedList<>();
    /**
     * This is the current difficulty
     */
    private int difficulty;

    @Override
    public int getDifficulty() {
        return this.difficulty;
    }

    @Override
    public void setDifficulty(int d) {
        int MAX_DIFFICULTY = 2;
        if (d <= MAX_DIFFICULTY) {
            this.difficulty = d;
        } else {
            System.err.println("Difficulty can't be set higher than " + MAX_DIFFICULTY);
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void addSelectedStone(Gem s) {
        if (this.toSwap.contains(s)) {
            this.toSwap.remove(s);
        } else {
            this.toSwap.add(s);
        }
        int MAX_SWAP_SIZE = 2;
        if (this.toSwap.size() > MAX_SWAP_SIZE) {
            System.err.println("toSwap can't have more than 2 elements");
            throw new IllegalStateException();
        }
    }

    @Override
    public List<Gem> getSelectedStones() {
        return this.toSwap;
    }
}
