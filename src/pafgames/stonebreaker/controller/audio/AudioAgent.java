package pafgames.stonebreaker.controller.audio;

import java.io.InputStream;

import pafgames.stonebreaker.app.StoneBreaker;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 * Class used to reproduce audio file given in resource folder.
 */
public class AudioAgent extends Thread {
    /**
     * Variable used for loop.
     */
    private final boolean loop;
    /**
     * Variable used for path.
     */
    private final String path;
    /**
     * Variable used for InputStream.
     */
    private InputStream file;
    /**
     * Variable used for Player.
     */
    private Player player;

    /**
     * @param path String that defines the path to the resources.
     * @param loop Boolean that decides if audio files are reproduced in loop.
     */
    public AudioAgent(final String path, final boolean loop) {
        this.path = path;
        this.file = StoneBreaker.class.getResourceAsStream(this.path);
        this.loop = loop;
        try {
            player = new Player(file);
        } catch (final JavaLayerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method run for thread.
     */
    public void run() {
        try {
            do {
                this.file = StoneBreaker.class.getResourceAsStream(this.path);
                this.player = new Player(file);
                this.player.play();
                this.player.close();
            } while (this.loop);
        } catch (final JavaLayerException e) {
            e.printStackTrace();
        }
    }
}
