package pafgames.stonebreaker.controller.game;

import java.util.Optional;

import pafgames.stonebreaker.model.Stone;

public interface Controller {
    /**
     * 
     * @return StoneListener.
     */
    Listener getStoneListener();
    /**
     * 
     * @return TimeListener.
     */
    Listener getTimeListener(final boolean stop);
    /**
     * 
     * @return DiffListener.
     */
    Listener getDifficultListener();
    /**
     * 
     * @return PowerListener.
     */
    Listener getPowerUpListener();
    /**
     * 
     * @return ModeListener.
     */
    Listener getModeListener();
    /**
     * 
     * @return Gameboard rows.
     */
    int getRow();
    /**
     * 
     * @return Gameboard column.
     */
    int getColumn();
    /**
     * 
     * @param index of Gem to give.
     * @return Gameboard as a list of Gem.
     */
    Optional<Stone> getStoneByIndex(final int index);
    /**
     * Set the board game in Arcade o Classic mode.
     */
    void setBoardGUI();
    /**
     * Set difficulty for player.
     * @param difficulty difficult
     */
    void setDifficulty(final int difficulty);
    /**
     * It stops time during the game.
     * @param stop true if you stop the game
     */    
    void setStopTime(final boolean stop);
    /**
     * Activate power up.
     */
    void setPower();
    /**
     * 
     * @param move true if is arcade mode.
     */
    void setMoveMode(final boolean move);
    /**
     * Define the sequence of actions to do when there is a swap,
     * also if power up is pressed.
     */
    void action();
}
