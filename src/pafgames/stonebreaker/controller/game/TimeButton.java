package pafgames.stonebreaker.controller.game;

import java.awt.event.ActionEvent;

/**
 * Class to be attached to a button in view.
 */
public class TimeButton implements Listener {
    /**
     * Field for controller.
     */
    private ControllerImpl controller;
    /**
     * Boolean field that defines if time has to be stopped.
     */
    private boolean stop;

    /**
     * @param stop true if time has to be stopped.
     */
    public TimeButton(final boolean stop) {
        this.stop = stop;
    }

    /**
     * @param contr a Controller.
     */
    @Override
    public void addListener(final ControllerImpl contr) {
        this.controller = contr;
    }

    /**
     * Behavior if button is pressed.
     */
    @Override
    public void actionPerformed(final ActionEvent arg) {
        this.controller.setStopTime(stop);
    }

}
