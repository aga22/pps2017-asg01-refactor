package pafgames.stonebreaker.controller.game;

import pafgames.stonebreaker.model.Stone;

public interface CheckCombo {
    /**
     * @param score is true if you can score.
     */
    void setCanScore(final boolean score);

    /**
     * @return true if power is activate.
     */
    boolean isPowerActive();

    /**
     * Update the score on the board.
     *
     * @param canScore true if score can be update.
     * @param counter  of occurence of a stone.
     */
    void update(final Stone gem, final boolean canScore, final int counter);

    /**
     * Check all the combo on the board.
     *
     * @return 0 if there are no more combo left.
     */
    int checkCombo();
}
