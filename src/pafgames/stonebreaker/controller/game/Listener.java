package pafgames.stonebreaker.controller.game;

import java.awt.event.ActionListener;

/**
 * Interface for actionListener in controller.
 */
public interface Listener extends ActionListener {
    /**
     * Add a controller that see view's behavior.
     *
     * @param contr a Controller.
     */
    void addListener(final ControllerImpl contr);
}
