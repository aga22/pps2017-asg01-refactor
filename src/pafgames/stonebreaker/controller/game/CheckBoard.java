package pafgames.stonebreaker.controller.game;

import java.util.List;

import pafgames.stonebreaker.model.Gem;

public interface CheckBoard {
    /**
     * @return current score.
     */
    int getScore();

    /**
     * @return activate Power.
     */
    boolean isPowerActive();

    /**
     * Initialize Gameboard.
     */
    void initialization();

    /**
     * Verify if two Gem can be swapped.
     *
     * @param list where there are gems to swap.
     * @return true if the gems are swapped.
     */
    boolean verifySwap(final List<Gem> list);

    /**
     * Method that verifies combos after a swap.
     *
     * @return 0 if there are not combo.
     */
    int checkCombo();

    /**
     * Mark blocks on last line of matrix to be deleted.
     *
     * @return true if there are block present.
     */
    boolean markBlocks();

    /**
     * Sequence of actions that destroys stones marked and repopulate the board.
     */
    void destroyStoneAndRepopulate();

    /**
     * Method used in power ups. Delete a square around a gem pressed.
     *
     * @param occurI X position of gem pressed.
     * @param occurJ Y position of gem pressed.
     */
    void deleteSquare(final int occurI, final int occurJ);
}
