package pafgames.stonebreaker.controller.game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * Class to be attached to a button in view.
 */
public class PowerListener implements ActionListener, Listener {
    /**
     * Field for controller.
     */
    private ControllerImpl controller;

    /**
     * @param contr a Controller.
     */
    @Override
    public void addListener(final ControllerImpl contr) {
        this.controller = contr;
    }

    /**
     * Behavior if button is pressed.
     */
    @Override
    public void actionPerformed(final ActionEvent arg) {
        final JButton button = (JButton) arg.getSource();
        button.setEnabled(false);
        this.controller.setPower();
    }

}
