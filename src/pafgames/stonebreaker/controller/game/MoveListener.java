package pafgames.stonebreaker.controller.game;

import java.awt.event.ActionEvent;

import pafgames.stonebreaker.view.utility.ModeButton;

/**
 * Class to be attached to a button in view.
 */
public class MoveListener implements Listener {
    /**
     * Integer for time mode.
     */
    private static final int TIMEMODE = 1;
    /**
     * Field for controller.
     */
    private ControllerImpl controller;

    /**
     * @param contr a Controller.
     */
    @Override
    public void addListener(final ControllerImpl contr) {
        this.controller = contr;
    }

    /**
     * Behavior if button is pressed.
     */
    @Override
    public void actionPerformed(final ActionEvent arg) {
        final ModeButton button = (ModeButton) arg.getSource();
        if (button.getMode() == TIMEMODE) {
            this.controller.setMoveMode(false);
        } else {
            this.controller.setMoveMode(true);
        }
    }

}
