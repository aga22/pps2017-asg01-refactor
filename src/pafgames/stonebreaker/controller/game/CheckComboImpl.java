package pafgames.stonebreaker.controller.game;

import java.util.Optional;

import pafgames.stonebreaker.model.GameBoard;
import pafgames.stonebreaker.model.Gem;
import pafgames.stonebreaker.model.Stone;
import pafgames.stonebreaker.model.scores.ScoreImpl;

/**
 * Class to check if there are combo.
 */
public class CheckComboImpl implements CheckCombo {
    /**
     * Limit for consequence stones.
     */
    private static final int LIMITOFCOMBO = 2;
    /**
     * Limit for consequence power.
     */
    private static final int LIMITFORPOWER = 4;
    /**
     * A GameBoard.
     */
    private final GameBoard gameboard;
    /**
     * A Score.
     */
    private final ScoreImpl score;
    /**
     * True if score can be updated.
     */
    private boolean canScore;
    /**
     * True if Power is activatePowerUp.
     */
    private boolean canActivate;

    /**
     * @param board the gameBoard.
     * @param score the score.
     */
    public CheckComboImpl(final GameBoard board, final ScoreImpl score) {
        this.gameboard = board;
        this.score = score;
        this.canActivate = false;
    }

    /**
     * @param score true if you can score.
     */
    @Override
    public void setCanScore(final boolean score) {
        this.canScore = score;
    }

    /**
     * @return true if power is activatePowerUp.
     */
    @Override
    public boolean isPowerActive() {
        return canActivate;
    }

    /**
     * @param canScore true if score can be update.
     * @param counter  of occurence of a stone.
     */
    @Override
    public void update(final Stone gem, final boolean canScore, final int counter) {
        if (canScore) {
            this.score.updateScore(gem, counter);
        }
    }

    /**
     * Check all the combo on the board.
     *
     * @return 0 if there are no more combo left.
     */
    @Override
    public int checkCombo() {
        int returnValue = 0;
        int counter;
        int offset = 0;
        this.canActivate = false;

        for (int i = 0; i < this.gameboard.getRows(); i++) {
            for (int j = 0; j < this.gameboard.getColumns(); j++) {
                Optional<Stone> stone = this.gameboard.getBoard().get(i).get(j);
                counter = checkPosition(Position.LEFT, stone);
                returnValue = makeMove(stone, counter, i, j, offset, returnValue);
                offset = 0;
                counter = checkPosition(Position.RIGHT, stone);
                returnValue = makeMove(stone, counter, i, j, offset, returnValue);
                offset = 0;
                counter = checkPosition(Position.UP, stone);
                returnValue = makeMove(stone, counter, i, j, offset, returnValue);
                offset = 0;
                counter = checkPosition(Position.DOWN, stone);
                returnValue = makeMove(stone, counter, i, j, offset, returnValue);
            }
        }
        return returnValue;
    }

    /**
     * Function to make the move
     *
     * @param counter number of occurences
     * @param i       row of the gameboard
     * @param j       col of the gameboard
     * @param offset  distance to slide gameboard
     * @param value   previous return value
     * @return new return value
     */
    private int makeMove(Optional<Stone> gem, int counter, int i, int j, int offset, int value) {
        int returnValue = value;
        if (counter >= LIMITOFCOMBO && gem.isPresent()) {
            this.update(gem.get(), this.canScore, counter);
            this.activatePowerUp(counter);
            returnValue++;
            while (counter != -1) {
                if (i + offset < this.gameboard.getRows() &&
                        this.gameboard.getBoard().get(i + offset).get(j).isPresent()) {
                    this.gameboard.getBoard().get(i + offset).get(j).get().deleteMe();
                }
                offset++;
                counter--;
            }
        }
        return returnValue;
    }

    /**
     * Active power up
     *
     * @param counter number of occurences.
     */
    private void activatePowerUp(final int counter) {
        if (counter >= LIMITFORPOWER) {
            this.canActivate = true;
        }
    }

    /**
     * Returns the right stone from given position
     *
     * @param position the enum of the position on the board
     * @param stone    the stone in the gameBoard
     * @return stone
     */
    private Optional<Stone> getStoneFromGameBoard(final Position position, final Optional<Stone> stone) {
        switch (position) {
            case LEFT:
                return this.gameboard.getLeftStone(stone);
            case RIGHT:
                return this.gameboard.getRightStone(stone);
            case UP:
                return this.gameboard.getUpStone(stone);
            case DOWN:
                return this.gameboard.getDownStone(stone);
        }
        return Optional.empty();
    }

    /**
     * Checks if stone near fro ìm the one who is considered
     * is equals
     *
     * @param position the enum of the position on the board
     * @param stone    the stone in the gameBoard
     * @return counter for stones
     */
    private int checkPosition(final Position position, final Optional<Stone> stone) {
        int counterStone = 0;
        Optional<Stone> stoneFromGameBoard = getStoneFromGameBoard(position, stone);
        if (stoneFromGameBoard.isPresent() &&
                stoneFromGameBoard.get() instanceof Gem &&
                stone.isPresent() && stone.get() instanceof Gem) {
            final Gem gem = (Gem) stoneFromGameBoard.get();
            final Gem gem2 = (Gem) stone.get();
            if (gem.getColour().equals(gem2.getColour())) {
                counterStone = checkPosition(position, stoneFromGameBoard) + 1;
            }
        }
        return counterStone;
    }
}
