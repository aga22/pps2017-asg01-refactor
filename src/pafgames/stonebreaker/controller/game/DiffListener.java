package pafgames.stonebreaker.controller.game;

import java.awt.event.ActionEvent;

import pafgames.stonebreaker.view.utility.DifficultButton;

/**
 * Class to be attached to a button in view.
 */
public class DiffListener implements Listener {
    /**
     * Field for controller.
     */
    private ControllerImpl controller;

    /**
     * @param contr a Controller.
     */
    @Override
    public void addListener(final ControllerImpl contr) {
        this.controller = contr;
    }

    /**
     * Behavior if button is pressed.
     */
    @Override
    public void actionPerformed(final ActionEvent arg) {
        final DifficultButton diffButton = (DifficultButton) arg.getSource();
        this.controller.setDifficulty(diffButton.getDifficult());
        this.controller.setBoardGUI();
    }

}
