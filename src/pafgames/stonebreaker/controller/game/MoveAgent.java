package pafgames.stonebreaker.controller.game;

import java.util.List;

import pafgames.stonebreaker.controller.audio.AudioAgent;
import pafgames.stonebreaker.model.mode.Counter;
import pafgames.stonebreaker.model.mode.CounterMove;
import pafgames.stonebreaker.view.board.BoardGUI;

/**
 * Thread class to define Move in the game.
 */
public class MoveAgent extends Thread {
    /**
     * Field for Board.
     */
    private final BoardGUI board;
    /**
     * Field for Counter
     */
    private final Counter counter;
    /**
     * Field used for object used to save ranking.
     */
    private final SaveRanking save;
    /**
     * Field for CheckBoard.
     */
    private final CheckBoard check;
    /**
     * Defines if time is moved or not.
     */
    private boolean isMoved;

    /**
     * @param board the gameBoard.
     * @param check object that defines board behavior.
     */
    public MoveAgent(final BoardGUI board, final CheckBoard check) {
        super();
        this.board = board;
        this.check = check;
        this.isMoved = false;
        this.counter = new CounterMove();
        this.save = new SaveRankingImpl();
    }

    /**
     * Method that defines thread behavior.
     */
    public void run() {
        while (!this.counter.isEnded()) {
            this.board.setTime(this.counter.getVal());
            if (this.isMoved) {
                this.isMoved = false;
                this.counter.dec();
            }
        }
        if (this.counter.isEnded()) {
            new AudioAgent("/Music/End.mp3", false).start();
            final List<Integer> list = this.save.getList(false);
            this.save.checkList(list, this.check.getScore());
            this.save.saveScore(list, false);
            this.board.setEnd(this.save.getList(false));
        }
    }

    /**
     * return true if isMoved.
     */
    synchronized void setMoved() {
        this.isMoved = true;
    }
}
