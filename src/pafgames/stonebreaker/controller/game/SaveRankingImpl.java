package pafgames.stonebreaker.controller.game;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 * Class used for save ranking on a file.
 *
 */
public class SaveRankingImpl implements SaveRanking {
    /**
     * Path for ranking in Classic mode.
     */
    private final String fileNameTimeMode = "." + System.getProperty("file.separator") + "scoresTimeMode.dat";
    /**
     * Path for ranking in Arcade mode.
     */
    private final String fileNameMoveMode = "." + System.getProperty("file.separator") + "scoresMoveMode.dat";
    /**
     * Limit for the list of rank.
     */
    private static final int LISTLIMIT = 3;
    /**
     * Check if elem can be added to the three elements of the list.
     * @param list the list to put on a file.
     * @param elem element to be add on the list.
     * @return the list to insert in file.
     */
    @Override
    public void checkList(final List<Integer> list, final int elem) {
        if(list.size() < LISTLIMIT) {
            list.add(elem);
            list.sort((x, y) -> y - x);
        } else {
            list.sort((x, y) -> y - x);
            if(list.get(LISTLIMIT - 1) < elem){
                list.remove(LISTLIMIT - 1);
                list.add(elem);
                list.sort((x, y) -> y - x);
            }
        }
    }
    /**
     * 
     * @param mode true for classic mode, false for arcade.
     * @return List read from file.
     */
    @Override
    public List<Integer> getList(final boolean mode) {
        return this.readScore(mode);
    }
    /**
     * Save score on a file;
     * @param list list
     * @param mode mode
     */
    @Override
    public void saveScore(final List<Integer> list, final boolean mode) {
        final ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(
                  new BufferedOutputStream(
                  new FileOutputStream(this.getString(mode))));
            oos.writeObject(list);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 
     * @param mode true if Time mode, false for Arcade.
     * @return read score from file, if it is not present return empty list.
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Integer> readScore(final boolean mode) {
        final ObjectInputStream ois;
        final List<Integer> list;
        try {
            ois = new ObjectInputStream(
                  new BufferedInputStream(
                  new FileInputStream(this.getString(mode))));
            list = (List<Integer>) ois.readObject();
            ois.close();
            return list;
        } catch (IOException | ClassNotFoundException e) {
            return new LinkedList<>();
        }
    }
    /**
     * 
     * @param mode true if Time mode, false for Arcade.
     * @return according to mode return path for file.
     */
    private String getString(final boolean mode) {
        return mode ? fileNameTimeMode : fileNameMoveMode;
    }
}