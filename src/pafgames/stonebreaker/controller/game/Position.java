package pafgames.stonebreaker.controller.game;

public enum Position {

    LEFT("Left"), RIGHT("Right"), UP("Up"), DOWN("Down");

    private final String text;

    /**
     * @param text text
     */
    Position(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
