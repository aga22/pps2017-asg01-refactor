package pafgames.stonebreaker.controller.game;

import java.util.List;

import pafgames.stonebreaker.controller.audio.AudioAgent;
import pafgames.stonebreaker.model.mode.Counter;
import pafgames.stonebreaker.model.mode.CounterTime;
import pafgames.stonebreaker.view.board.BoardGUI;

/**
 * Thread class to define Time in the game.
 */
public class TimeAgent extends Thread {
    /**
     * Field for Board.
     */
    private final BoardGUI board;
    /**
     * Field for Counter
     */
    private final Counter counter;
    /**
     * Field used for object used to save ranking.
     */
    private final SaveRanking save;
    /**
     * Field for CheckBoard.
     */
    private final CheckBoard check;
    /**
     * Defines if time is stopped or not.
     */
    private boolean isStopped;

    /**
     * @param board the gameBoard.
     * @param check object that defines board behavior.
     */
    public TimeAgent(final BoardGUI board, final CheckBoard check) {
        super();
        this.isStopped = false;
        this.board = board;
        this.check = check;
        this.counter = new CounterTime();
        this.save = new SaveRankingImpl();
    }

    /**
     * Method that defines thread behavior.
     */
    public void run() {
        while (!this.counter.isEnded()) {
            this.board.setTime(this.counter.getVal());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!this.isStopped) {
                this.counter.dec();
            }
        }
        if (this.counter.isEnded()) {
            new AudioAgent("/Music/End.mp3", false).start();
            final List<Integer> list = this.save.getList(true);
            this.save.checkList(list, this.check.getScore());
            this.save.saveScore(list, true);
            this.board.setEnd(this.save.getList(true));
        }
    }

    /**
     * @param stop true if you want to stop time.
     */
    synchronized void setStop(final boolean stop) {
        this.isStopped = stop;
    }
}
