package pafgames.stonebreaker.controller.game;

import java.util.List;

public interface SaveRanking {

    /**
     * Check if elem can be added to the three elements of the list.
     *
     * @param list the list to put on a file.
     * @param elem element to be add on the list.
     */
    void checkList(final List<Integer> list, final int elem);

    /**
     * @param mode true for classic mode, false for arcade.
     * @return List read from file.
     */
    List<Integer> getList(final boolean mode);

    /**
     * Save score on a file;
     *
     * @param list list
     * @param mode game mode
     */
    void saveScore(final List<Integer> list, final boolean mode);

    /**
     * @param mode true if Time mode, false for Arcade.
     * @return read score from file, if it is not present return empty list.
     */
    List<Integer> readScore(final boolean mode);
}
