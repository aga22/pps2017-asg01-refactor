package pafgames.stonebreaker.controller.game;

import pafgames.stonebreaker.model.*;
import pafgames.stonebreaker.model.scores.ScoreImpl;
import pafgames.stonebreaker.model.scores.ScoreStrategy;

import java.util.List;

/**
 * Class used to define controls on gameboard.
 */
public class CheckBoardImpl implements CheckBoard {
    /**
     * Points for a power up.
     */
    private static final int SQUAREPOINTS = 9;
    /**
     * A gameboard.
     */
    private final GameBoard gameboard;
    /**
     * A score used to update it.
     */
    private final ScoreImpl score;
    /**
     * A field for check combo.
     */
    private final CheckCombo combo;

    /**
     * @param gameboard used in the gameplay.
     */
    public CheckBoardImpl(final GameBoard gameboard, final ScoreStrategy strategy) {
        this.gameboard = gameboard;
        this.score = new ScoreImpl(strategy);
        this.combo = new CheckComboImpl(this.gameboard, this.score);
    }

    /**
     * @return current score.
     */
    @Override
    public int getScore() {
        return this.score.getScore();
    }

    /**
     * @return activate Power.
     */
    @Override
    public boolean isPowerActive() {
        return this.combo.isPowerActive();
    }

    /**
     * Initialize Gameboard.
     */
    @Override
    public void initialization() {
        int ret;
        boolean isBlock;
        this.gameboard.repopulate();
        do {
            ret = this.checkCombo();
            isBlock = markBlocks();
            this.gameboard.destroyMarked();
            this.gameboard.fallDown();
            this.gameboard.repopulate();
        } while (ret != 0 || isBlock);
        this.combo.setCanScore(true);
    }

    /**
     * Verify if two Gem can be swapped.
     *
     * @param list where there are gems to swap.
     * @return true if the gems are swapped.
     */
    @Override
    public boolean verifySwap(final List<Gem> list) {
        if (list.size() == 2 && this.gameboard.isNeighbour(list.get(0), list.get(1))) {
            this.gameboard.swap(list.get(0), list.get(1));
            list.clear();
            return true;
        } else if (list.size() == 1) {
            return false;
        } else {
            list.clear();
            return false;
        }
    }

    /**
     * Method that verifies combos after a swap.
     *
     * @return 0 if there are not combo.
     */
    @Override
    public int checkCombo() {
        return this.combo.checkCombo();
    }

    /**
     * Mark blocks on last line of matrix to be deleted.
     *
     * @return true if there are block present.
     */
    @Override
    public boolean markBlocks() {
        int last = this.gameboard.getRows();
        boolean isBlockPresent = false;
        for (int i = 0; i < this.gameboard.getBoard().get(last - 1).size(); i++) {
            if (this.gameboard.getBoard().get(last - 1).get(i).isPresent() &&
                    this.gameboard.getBoard().get(last - 1).get(i).get() instanceof Block) {
                this.gameboard.getBoard().get(last - 1).get(i).get().deleteMe();
                isBlockPresent = true;
            }
        }
        return isBlockPresent;
    }

    /**
     * Sequence of actions that destroys stones marked and repopulate the board.
     */
    @Override
    public void destroyStoneAndRepopulate() {
        this.gameboard.destroyMarked();
        this.gameboard.fallDown();
        this.initialization();
    }

    /**
     * Method used in power ups. Delete a square around a gem pressed.
     *
     * @param occurI X position of gem pressed.
     * @param occurJ Y position of gem pressed.
     */
    @Override
    public void deleteSquare(final int occurI, final int occurJ) {
        for (int i = occurI - 1; i <= occurI + 1; i++) {
            for (int j = occurJ - 1; j <= occurJ + 1; j++) {
                if ((i - 1 >= 0 && j >= 0 &&
                        i < this.gameboard.getColumns() && j < this.gameboard.getRows()) &&
                        this.gameboard.getBoard().get(i).get(j).isPresent()) {
                    this.gameboard.getBoard().get(i).get(j).get();
                    this.gameboard.getBoard().get(i).get(j).get().deleteMe();
                }
            }
        }
        this.combo.update(new Gem(-1,-1, -1), true, SQUAREPOINTS);
    }
}