package pafgames.stonebreaker.controller.game;

import java.util.List;
import java.util.Optional;

import pafgames.stonebreaker.controller.audio.AudioAgent;
import pafgames.stonebreaker.model.GameBoard;
import pafgames.stonebreaker.model.GameBoardImpl;
import pafgames.stonebreaker.model.Gem;
import pafgames.stonebreaker.model.Player;
import pafgames.stonebreaker.model.Stone;
import pafgames.stonebreaker.model.scores.AdvScoreStrategy;
import pafgames.stonebreaker.model.scores.SimpleScoreStrategy;
import pafgames.stonebreaker.view.board.BoardGUI;
import pafgames.stonebreaker.view.board.BoardGUIImpl;

/**
 * Controller class used to merge view and model.
 */
public class ControllerImpl implements Controller {
    /**
     * A field for Gameboard.
     */
    private final GameBoard gameboard;
    /**
     * A field for Player.
     */
    private final Player player;
    /**
     * A field for CheckBoard.
     */
    private final CheckBoard check;
    /**
     * A time Agent.
     */
    private TimeAgent timeAgent;
    /**
     * A Move Agent.
     */
    private MoveAgent moveAgent;
    /**
     * A board.
     */
    private BoardGUI board;
    /**
     * Boolean to see if power is pressed.
     */
    private boolean isPowerPressed;
    /**
     * Boolean true if is move mode.
     */
    private boolean isMoveMode;

    /**
     * Constructor.
     */
    public ControllerImpl() {
        this.gameboard = new GameBoardImpl();
        this.check = new CheckBoardImpl(this.gameboard, new AdvScoreStrategy());
        this.player = this.gameboard.getPlayer();
    }

    /**
     * @return StoneListener.
     */
    @Override
    public Listener getStoneListener() {
        final Listener stoneListener = new SaveListener(this.player);
        stoneListener.addListener(this);
        return stoneListener;
    }

    /**
     * @return TimeListener.
     */
    @Override
    public Listener getTimeListener(final boolean stop) {
        final Listener button = new TimeButton(stop);
        button.addListener(this);
        return button;
    }

    /**
     * @return DiffListener.
     */
    @Override
    public Listener getDifficultListener() {
        final Listener diff = new DiffListener();
        diff.addListener(this);
        return diff;
    }

    /**
     * @return PowerUpListener.
     */
    @Override
    public Listener getPowerUpListener() {
        final Listener pUp = new PowerListener();
        pUp.addListener(this);
        return pUp;
    }

    /**
     * @return ModeListener.
     */
    @Override
    public Listener getModeListener() {
        final Listener mode = new MoveListener();
        mode.addListener(this);
        return mode;
    }

    /**
     * @return Gameboard rows.
     */
    @Override
    public int getRow() {
        return this.gameboard.getRows();
    }

    /**
     * @return Gameboard columns.
     */
    @Override
    public int getColumn() {
        return this.gameboard.getColumns();
    }

    /**
     * @param index of Gem to give.
     * @return Gameboard as a list of Gem.
     */
    @Override
    public Optional<Stone> getStoneByIndex(final int index) {
        return this.gameboard.getBoardAsList().get(index);
    }

    /**
     * Set the board game in Arcade o Classic mode.
     */
    @Override
    public void setBoardGUI() {
        this.check.initialization();
        this.board = new BoardGUIImpl(this);
        if (this.isMoveMode) {
            this.moveAgent = new MoveAgent(this.board, this.check);
            this.moveAgent.start();
        } else {
            this.timeAgent = new TimeAgent(this.board, this.check);
            this.timeAgent.start();
        }
    }

    /**
     * Set difficulty for player.
     *
     * @param difficulty difficoltà
     */
    @Override
    public void setDifficulty(final int difficulty) {
        this.player.setDifficulty(difficulty);
    }

    /**
     * It stops time during the game.
     *
     * @param stop to stop game
     */
    @Override
    public void setStopTime(final boolean stop) {
        if (!this.isMoveMode) {
            this.timeAgent.setStop(stop);
        }
    }

    /**
     * Activate power up.
     */
    @Override
    public void setPower() {
        this.isPowerPressed = true;
    }

    /**
     * @param move true if is arcade mode.
     */
    @Override
    public void setMoveMode(final boolean move) {
        this.isMoveMode = move;
    }

    /**
     * Define the sequence of actions to do when there is a swap,
     * also if power up is pressed.
     */
    @Override
    public void action() {
        List<Gem> list = this.player.getSelectedStones();
        if (this.isPowerPressed && list.size() == 1) {
            this.isPowerPressed = false;
            this.check.deleteSquare(list.get(0).getX(), list.get(0).getY());
            list.clear();
            this.check.destroyStoneAndRepopulate();
            if (this.isMoveMode) {
                this.moveAgent.setMoved();
            }
            this.board.updateScore(this.check.getScore());
            new AudioAgent("/Music/Power.mp3", false).start();
            this.board.updateView();
        } else if (this.check.verifySwap(list)) {
            if (this.isMoveMode) {
                this.moveAgent.setMoved();
            }
            new AudioAgent("/Music/Swap.mp3", false).start();
            this.board.updateView();
            this.check.checkCombo();
            if (this.check.isPowerActive()) {
                this.board.enablePowerUp();
            }
            this.check.markBlocks();
            this.check.destroyStoneAndRepopulate();
            this.board.updateScore(this.check.getScore());
            this.board.updateView();
        }
    }
}