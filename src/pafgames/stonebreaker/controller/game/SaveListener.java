package pafgames.stonebreaker.controller.game;

import java.awt.event.ActionEvent;

import pafgames.stonebreaker.model.Gem;
import pafgames.stonebreaker.model.Player;
import pafgames.stonebreaker.view.utility.StonesButton;

/**
 * Class to be attached to a button in view.
 */
public class SaveListener implements Listener {
    /**
     * Field for controller.
     */
    private ControllerImpl controller;
    /**
     * Field for player.
     */
    private Player player;

    /**
     * @param player a Player.
     */
    public SaveListener(final Player player) {
        this.player = player;
    }

    /**
     * @param contr a Controller.
     */
    @Override
    public void addListener(final ControllerImpl contr) {
        this.controller = contr;
    }

    /**
     * Behavior if button is pressed.
     */
    @Override
    public void actionPerformed(final ActionEvent arg) {

        final StonesButton stoneButton = (StonesButton) arg.getSource();
        if (this.controller.getStoneByIndex(stoneButton.getIndex()).isPresent() &&
                this.controller.getStoneByIndex(stoneButton.getIndex()).get() instanceof Gem) {
            final Gem gem = (Gem) this.controller.getStoneByIndex(stoneButton.getIndex()).get();
            this.player.addSelectedStone(gem);
            this.controller.action();
        }
    }
}