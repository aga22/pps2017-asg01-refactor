package pafgames.stonebreaker.app;

import pafgames.stonebreaker.view.menu.SplashScreen;

/**
 * App main.
 */
public class StoneBreaker {

    /**
     * app main.
     *
     * @param args args.
     */
    public static void main(final String... args) {
        new SplashScreen();
    }
}
