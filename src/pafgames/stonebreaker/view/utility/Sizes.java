package pafgames.stonebreaker.view.utility;

/**
 * Class used to organize screen sizes.
 */
public class Sizes {

    /**
     * value for screen minimum width.
     */
    public static final int MIN_WIDTH = 550;
    /**
     * value for screen minimum height.
     */
    public static final int MIN_HEIGHT = 700;
    /**
     * value for screen default width.
     */
    public static final int DEF_WIDTH = 900;
    /**
     * value for screen default height.
     */
    public static final int DEF_HEIGHT = 700;
    /**
     * value for screen tutorial proportions.
     */
    public static final int TUTORIAL_SIZE = 700;
    /**
     * value for screen intermediate window's width.
     */
    public static final int INT_WIDHT = 600;
    /**
     * value for screen intermediate window's height.
     */
    public static final int INT_HEIGHT = 300;
    /**
     * value for screen JWindow's width.
     */
    public static final int WIN_WIDHT = 800;
    /**
     * value for screen JWindow's height.
     */
    public static final int WIN_HEIGHT = 600;
    /**
     * value for screen pause window proportions..
     */
    public static final int PAUSE = 500;
    /**
     * value for screen max width.
     */
    public static final int MAX_W = 1920;
    /**
     * value for screen max height.
     */
    public static final int MAX_H = 1080;

    /**
     * private constructor.
     */
    private Sizes() {
    }
}
