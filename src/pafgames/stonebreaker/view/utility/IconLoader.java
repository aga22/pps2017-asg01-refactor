package pafgames.stonebreaker.view.utility;

import javax.swing.ImageIcon;

import pafgames.stonebreaker.view.board.BoardGUIImpl;

/**
 * Class used to load easily icons for stonesButton.
 */
public enum IconLoader {

    /**
     * Red stone.
     */
    RED("Red"),
    /**
     * Purple stone.
     */
    PURPLE("Purple"),
    /**
     * Yellow stone.
     */
    YELLOW("Yellow"),
    /**
     * Green stone.
     */
    GREEN("Green"),
    /**
     * Blue stone.
     */
    CYAN("Cyan"),
    /**
     * White stone.
     */
    WHITE("White"),
    /**
     * Block.
     */
    STONE("Black");

    /**
     * Name of the image.
     */
    private String s;

    /**
     * Constructor of the enum.
     *
     * @param s the name of the image to set as icon.
     */
    IconLoader(final String s) {
        this.s = s;
    }

    /**
     * method used to load icon.
     *
     * @return the icon to set at the JButton.
     */
    public ImageIcon getIcon() {
        return new ImageIcon(BoardGUIImpl.class.getResource("/Images/" + this.s + ".png"));
    }
}
