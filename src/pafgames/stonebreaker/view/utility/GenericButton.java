package pafgames.stonebreaker.view.utility;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import pafgames.stonebreaker.controller.audio.AudioAgent;
import pafgames.stonebreaker.view.board.BoardGUIImpl;

/**
 * GenericButton used to show animations.
 */
public class GenericButton extends JButton{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * images extension.
	 */
	private static final String END_STRING = ".png";
	
	/**
	 * Constructor of GenericButton.
	 * @param s
	 * 		the name of the image to load.
	 * - necessary an image with same name but char "2" final.
	 * In order to get animation.
	 */
	public GenericButton(final String s) {
		this.setBorderPainted(false);
		this.setFocusPainted(false);
		this.setFocusPainted(false);
		this.setContentAreaFilled(false);
		this.setIcon(new ImageIcon(BoardGUIImpl.class.getResource("/Images/" + s + "2" + END_STRING)));
		this.setSize(this.getPreferredSize());
		this.addActionListener(event -> new AudioAgent("/Music/Click.mp3", false).start());
		this.addMouseListener(new MouseAdapter() {
			public void mouseEntered(final MouseEvent evt) {
				GenericButton.this.setIcon(new ImageIcon(BoardGUIImpl.class.getResource("/Images/" + s + END_STRING)));
            }
			public void mouseExited(final MouseEvent evt) {
				GenericButton.this.setIcon(new ImageIcon(BoardGUIImpl.class.getResource("/Images/" + s + "2" + END_STRING)));
            }
		});
	}
}
