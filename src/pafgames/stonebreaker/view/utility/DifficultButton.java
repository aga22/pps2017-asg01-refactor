package pafgames.stonebreaker.view.utility;

/**
 * Button used to select difficulty.
 */
public class DifficultButton extends GenericButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * field used to getDifficulty selected.
     */
    private final int difficult;

    /**
     * @param s         String to get GenericButton constructor.
     * @param difficult Difficult set for the JButton.
     */
    public DifficultButton(final String s, final int difficult) {
        super(s);
        this.difficult = difficult;
    }

    /**
     * this is the function called to get the difficulty stored
     * in the JButton.
     *
     * @return int.
     * the difficult.
     */
    public int getDifficult() {
        return this.difficult;
    }
}
