package pafgames.stonebreaker.view.utility;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 * class that models the game STONES and BLOCKS.
 */
public class StonesButton extends JButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * personal index.
     */
    private int index;

    /**
     * constructor of SaveListener.
     *
     * @param i the STONES/BLOCK index.
     */
    public StonesButton(final int i) {
        this.index = i;
        this.setBorderPainted(false);
        this.setBorder(BorderFactory.createRaisedBevelBorder());
        this.addActionListener(e -> this.setFocusPainted(true));
        this.setContentAreaFilled(false);
        this.setFocusPainted(false);
        this.addMouseListener(new MouseAdapter() {
            public void mouseEntered(final MouseEvent evt) {
                StonesButton.this.setBorderPainted(true);
            }

            public void mouseExited(final MouseEvent evt) {
                StonesButton.this.setBorderPainted(false);
            }
        });
    }

    /**
     * method to get SaveListener index.
     *
     * @return index of SaveListener.
     */
    public int getIndex() {
        return this.index;
    }
}
