package pafgames.stonebreaker.view.utility;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import pafgames.stonebreaker.app.StoneBreaker;

/**
 * class used to load images.
 */
public class ImageLoader {

    /**
     * private constructor.
     */
    private ImageLoader() {
    }

    /**
     * static function to load Gif res.
     *
     * @param pth path where the gif is contained.
     * @return the JLabel with the Gif.
     */
    public static JLabel loadGif(final String pth) {
        return new JLabel(new ImageIcon(StoneBreaker.class.getResource(pth)));
    }

    /**
     * static function used to load an image.
     *
     * @param pth the path where the image is contained.
     * @return the BufferedImage.
     */
    static BufferedImage loadImage(final String pth) {
        try {
            return ImageIO.read(ImageLoader.class.getResourceAsStream(pth));
        } catch (IOException e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
    }

}
