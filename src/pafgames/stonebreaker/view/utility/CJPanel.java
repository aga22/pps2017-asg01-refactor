package pafgames.stonebreaker.view.utility;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * Custom JPanel that support resize.
 * Used to customize view JPanels.
 */
public class CJPanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Image used as background image.
     */
    private final Image image;

    /**
     * Constructor of CJPanel.
     *
     * @param image the name of the image loaded in background.
     */
    public CJPanel(final String image) {
        this.setOpaque(false);
        this.image = ImageLoader.loadImage("/Images/" + image);
    }

    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        Dimension d = getSize();
        g.drawImage(image, 0, 0, d.width, d.height, null);
    }

}
