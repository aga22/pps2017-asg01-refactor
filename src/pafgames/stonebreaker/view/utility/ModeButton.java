package pafgames.stonebreaker.view.utility;

/**
 * Custom Generic Button used to select modality.
 */
public class ModeButton extends GenericButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * the modality of the game.
     */
    private int mode;

    /**
     * Constructor of the JButton.
     *
     * @param s    name of the image to load on GenericButton.
     * @param mode modality set for the game.
     */
    public ModeButton(final String s, final int mode) {
        super(s);
        this.mode = mode;
    }

    /**
     * function to get the modality set.
     *
     * @return the value of modality.
     */
    public int getMode() {
        return this.mode;
    }
}
