package pafgames.stonebreaker.view.menu;

import java.awt.BorderLayout;

import javax.swing.JWindow;

import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.GenericButton;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * This is the tutorial window, which shows
 * how to play the game.
 */
public class TutorialWindow extends JWindow {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * This is the button used to go back to the main menu.
     */
    private GenericButton backButton = new GenericButton("BackButton");

    /**
     * this is the TutorialWindow constructor.
     */
    public TutorialWindow() {
        this.setAlwaysOnTop(true);
        this.setSize(Sizes.TUTORIAL_SIZE, Sizes.TUTORIAL_SIZE);
        this.add(new TutorialPanel());
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.backButton.addActionListener(e -> TutorialWindow.this.dispose());
    }

    /**
     * This is the main panel of the TutorialWindow.
     */
    private class TutorialPanel extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * constructor of main panel.
         */
        public TutorialPanel() {
            super("HowTo.png");
            this.setLayout(new BorderLayout());
            this.add(TutorialWindow.this.backButton, BorderLayout.LINE_END);
        }
    }
}
