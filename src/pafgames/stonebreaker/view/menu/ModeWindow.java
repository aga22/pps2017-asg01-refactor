package pafgames.stonebreaker.view.menu;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.geom.Ellipse2D;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.JWindow;

import pafgames.stonebreaker.controller.game.Controller;
import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.ModeButton;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * this class models the JWindow used to choose game modality.
 */
public class ModeWindow extends JWindow {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * Number of modalities.
     */
    private static final int NUM_MOD = 2;

    /**
     * this is the Controller entity of the Game.
     */
    private final Controller controller;

    /**
     * List of custom JButton based on NUM_MOD.
     */
    private List<ModeButton> modes = new LinkedList<>();

    /**
     * Constructor of ModeWindow.
     *
     * @param controller the Controller entity of the Game.
     */
    public ModeWindow(final Controller controller) {
        this.controller = controller;
        this.setAlwaysOnTop(true);
        this.setSize(Sizes.INT_WIDHT, Sizes.INT_HEIGHT);
        this.setShape(new Ellipse2D.Double(0, 0, getWidth(), getHeight()));
        this.add(new ModePanel());
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     * This is the main panel of the ModeWindow.
     */
    private class ModePanel extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * Constructor of ModePanel.
         */
        public ModePanel() {
            super("BackBlack.jpeg");
            this.setLayout(new GridBagLayout());
            GridBagConstraints grid = new GridBagConstraints();
            grid.gridy = 0;
            IntStream.range(0, NUM_MOD).forEach(e -> {
                grid.gridx = e;
                ModeWindow.this.modes.add(new ModeButton("M" + e, e + 1));
                ModeWindow.this.modes.get(e).addActionListener(event -> {
                    new IntermediateWindow(ModeWindow.this.controller);
                    ModeWindow.this.dispose();
                });
                ModeWindow.this.modes.get(e).addActionListener(ModeWindow.this.controller.getModeListener());
                ModePanel.this.add(ModeWindow.this.modes.get(e), grid);
            });
        }
    }
}
