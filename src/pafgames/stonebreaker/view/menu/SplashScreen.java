package pafgames.stonebreaker.view.menu;

import java.awt.BorderLayout;

import javax.swing.JWindow;

import pafgames.stonebreaker.controller.audio.AudioAgent;
import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.ImageLoader;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * SplashScreen of the Game.
 */
public class SplashScreen extends JWindow {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * time of wait for splashScreen.
     */
    private static final int WAIT = 4000;
    /**
     * this is the background audio of the game.
     */
    private final AudioAgent intro = new AudioAgent("/Music/Background.mp3", true);

    public SplashScreen() {
        this.setSize(Sizes.WIN_WIDHT, Sizes.WIN_HEIGHT);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());
        this.add(new MenuPanel(), BorderLayout.CENTER);
        this.setVisible(true);
        this.startMusic();
        try {
            Thread.sleep(WAIT);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.dispose();
        new GameWindow();
    }

    /**
     * private method to start audioAgent thread.
     */
    private void startMusic() {
        this.intro.start();
    }

    /**
     * This is the main panel of the SplashScreen.
     */
    private class MenuPanel extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * Public constructor of MenuPanel.
         */
        public MenuPanel() {
            super("BackBlack.jpeg");
            this.setLayout(new BorderLayout());
            this.add(ImageLoader.loadGif("/Images/title.png"), BorderLayout.NORTH);
            this.add(ImageLoader.loadGif("/Images/spiral.gif"), BorderLayout.CENTER);
            this.add(ImageLoader.loadGif("/Images/loading.gif"), BorderLayout.SOUTH);
        }
    }
}
