package pafgames.stonebreaker.view.menu;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.geom.Ellipse2D;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.JWindow;

import pafgames.stonebreaker.controller.game.Controller;
import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.DifficultButton;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * Class used to show the intermediate Window where to choose
 * difficulty for the game.
 */
public class IntermediateWindow extends JWindow {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Number of difficulties.
     */
    private static final int NUM_DIFF = 3;

    /**
     * List of JButton based on difficulties size.
     */
    private List<DifficultButton> difficult = new LinkedList<>();

    /**
     * Controller entity of the game.
     */
    private Controller controller;

    /**
     * Constructor
     * of the IntermediateWindow.
     *
     * @param controller the controller that organize game before starting.
     */
    public IntermediateWindow(final Controller controller) {
        this.controller = controller;
        this.setAlwaysOnTop(true);
        this.setSize(Sizes.INT_WIDHT, Sizes.INT_HEIGHT);
        this.setShape(new Ellipse2D.Double(0, 0, getWidth(), getHeight()));
        this.add(new IntermediatePanel());
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     * The main GUI JPanel, which contains
     * components.
     */
    private class IntermediatePanel extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * Constructor of Custom Intermediate Panel.
         */
        public IntermediatePanel() {
            super("BackBlack.jpeg");
            this.setLayout(new GridBagLayout());
            GridBagConstraints grid = new GridBagConstraints();
            grid.gridx = 0;
            IntStream.range(0, NUM_DIFF).forEach(e -> {
                grid.gridy = e;
                IntermediateWindow.this.difficult.add(new DifficultButton(String.valueOf(e), e));
                IntermediateWindow.this.difficult.get(e).addActionListener(event -> IntermediateWindow.this.dispose());
                IntermediateWindow.this.difficult.get(e).addActionListener(IntermediateWindow.this.controller.getDifficultListener());
                IntermediatePanel.this.add(IntermediateWindow.this.difficult.get(e), grid);
            });
        }
    }
}
