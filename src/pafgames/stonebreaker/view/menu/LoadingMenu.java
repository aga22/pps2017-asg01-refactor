package pafgames.stonebreaker.view.menu;

import java.awt.BorderLayout;

import javax.swing.JWindow;

import pafgames.stonebreaker.controller.audio.AudioAgent;
import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.ImageLoader;

public class LoadingMenu extends JWindow {
    /**
     * LONGUID
     */
    private static final long serialVersionUID = 1L;

    public LoadingMenu() {
        AudioAgent intro = new AudioAgent("/Music/Background.mp3", false);
        intro.start();
        this.setSize(800, 600);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());
        this.add(new MenuPanel(), BorderLayout.CENTER);
        this.requestFocus();
        this.setVisible(true);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.dispose();
        new GameWindow();
    }

    private class MenuPanel extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public MenuPanel() {
            super("BackBlack.jpeg");
            this.setLayout(new BorderLayout());
            this.add(ImageLoader.loadGif("/Images/title.png"), BorderLayout.NORTH);
            this.add(ImageLoader.loadGif("/Images/spiral.gif"), BorderLayout.CENTER);
            this.add(ImageLoader.loadGif("/Images/loading.gif"), BorderLayout.SOUTH);
        }
    }
}
