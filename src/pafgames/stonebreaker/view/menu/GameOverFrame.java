package pafgames.stonebreaker.view.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.geom.Ellipse2D;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.JLabel;
import javax.swing.JWindow;

import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.GenericButton;
import pafgames.stonebreaker.view.utility.ImageLoader;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * JWindow visible when the game ends.
 */
public class GameOverFrame extends JWindow {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Used to set distance between components.
     */
    private static final int DISTANCE = 30;
    /**
     * JLabel used to show score.
     */
    private final JLabel overLabel = new JLabel();

    /**
     * List of Integer -- actual leader board.
     */
    private final List<Integer> saves;

    /**
     * JPanel that contains the sub-components.
     */
    private final GameOverPanel gOverPanel;

    /**
     * List of JLabel -- based on Saves.
     */
    private final List<JLabel> savesLabel = new LinkedList<>();

    /**
     * Constructor of GameOver JFrame.
     *
     * @param score just ended match's score.
     * @param saves list of precedents scores.
     */
    public GameOverFrame(final String score, final List<Integer> saves) {
        this.saves = saves;
        final Font myFont = new Font("SansSerif", Font.BOLD, 50);
        final Font myFont2 = new Font("SansSerif", Font.BOLD, 25);
        final Font myFont3 = new Font("SansSerif", Font.BOLD, 35);

        this.overLabel.setForeground(Color.red.darker());
        this.overLabel.setFont(myFont);
        this.overLabel.setText(score);

        IntStream.range(0, this.saves.size()).forEach(i -> {
            this.savesLabel.add(new JLabel());
            this.savesLabel.get(i).setForeground(Color.blue.darker());
            this.savesLabel.get(i).setFont(myFont2);
            if (i == 0) {
                this.savesLabel.get(i).setFont(myFont3);
            }
            this.savesLabel.get(i).setText(i + 1 + ". " + this.saves.get(i));
        });

        this.gOverPanel = new GameOverPanel();
        this.add(this.gOverPanel);
        this.setSize(Sizes.WIN_WIDHT, Sizes.WIN_HEIGHT);
        this.setShape(new Ellipse2D.Double(0, 0, getWidth(), getHeight()));
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        this.setVisible(true);
    }

    /**
     * This is the GameOver JWindow main panel.
     */
    private class GameOverPanel extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * Button used to go back to the main menu.
         */
        private GenericButton exitButton = new GenericButton("ExitButton");

        /**
         * Constructor of GameOver Panel.
         */
        public GameOverPanel() {
            super("BackBlack.jpeg");
            this.setLayout(new GridBagLayout());
            GridBagConstraints grid = new GridBagConstraints();
            grid.gridx = 1;
            grid.gridy = 0;
            this.add(ImageLoader.loadGif("/Images/GameOver.gif"), grid);
            grid.ipady = DISTANCE;
            grid.gridx = 1;
            grid.gridy = 1;
            this.add(ImageLoader.loadGif("/Images/Score.png"), grid);
            grid.ipady = 0;
            grid.gridx = 1;
            grid.gridy = 2;
            this.add(GameOverFrame.this.overLabel, grid);
            grid.gridx = 0;
            grid.gridy = 1;
            this.add(this.exitButton, grid);
            grid.gridx = 2;
            IntStream
                    .range(0, GameOverFrame.this.saves.size())
                    .forEach(i -> {
                        grid.gridy = 2 + i;
                        this.add(GameOverFrame.this.savesLabel.get(i), grid);
                    });
            grid.gridy = 1;
            this.add(ImageLoader.loadGif("/Images/Ranking.png"), grid);
            this.exitButton.addActionListener(e -> {
                GameOverFrame.this.dispose();
                new GameWindow();
            });
        }
    }
}
