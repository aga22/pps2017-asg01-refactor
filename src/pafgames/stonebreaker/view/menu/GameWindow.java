package pafgames.stonebreaker.view.menu;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;

import pafgames.stonebreaker.controller.game.Controller;
import pafgames.stonebreaker.controller.game.ControllerImpl;
import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.GenericButton;
import pafgames.stonebreaker.view.utility.ImageLoader;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * JFrame used as main menu for the application.
 */
public class GameWindow extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * used to set gridBagLayout.
     */
    private static final int LET = 3;
    /**
     * used to set gridBagLayout.
     */
    private static final int DIST = 70;
    /**
     * Instance of the Controller.
     */
    private final Controller controller = new ControllerImpl();

    /**
     *
     */
    public GameWindow() {
        this.setSize(Sizes.DEF_WIDTH, Sizes.DEF_HEIGHT);
        this.setMinimumSize(new Dimension(Sizes.MIN_WIDTH, Sizes.MIN_HEIGHT));
        this.setMaximumSize(new Dimension(Sizes.MAX_W, Sizes.MAX_H));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        this.setLocationRelativeTo(null);
        this.add(new FirstPage(), BorderLayout.CENTER);
        this.setVisible(true);
    }

    /**
     * This is the main menu main custom Panel.
     */
    private class FirstPage extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * Exit button used to exit the game.
         */
        private GenericButton exitButton;

        /**
         * Start button used to start the game.
         */
        private GenericButton startButton;

        /**
         * JButton used to show the Tutorial JWindow.
         */
        private GenericButton howToButton;

        /**
         * Constructor of the main panel.
         */
        public FirstPage() {
            super("BackBlack.jpeg");

            this.startButton = new GenericButton("StartButton");
            this.exitButton = new GenericButton("ExitButton");
            this.howToButton = new GenericButton("Tutorial");

            this.startButton.addActionListener(e -> {
                new ModeWindow(GameWindow.this.controller);
                GameWindow.this.dispose();
            });
            this.exitButton.addActionListener(e -> System.exit(0));
            this.howToButton.addActionListener(e -> new TutorialWindow());
            this.init();
        }

        /**
         * Method used to organize components.
         */
        private void init() {
            this.setLayout(new GridBagLayout());
            GridBagConstraints grid = new GridBagConstraints();
            grid.gridx = 0;
            grid.gridy = 0;
            grid.ipady = DIST;
            this.add(ImageLoader.loadGif("/Images/title.png"), grid);
            grid.ipady = 0;
            grid.gridy = 1;
            this.add(this.startButton, grid);
            grid.gridy = 2;
            this.add(this.howToButton, grid);
            grid.gridy = LET;
            this.add(this.exitButton, grid);
        }
    }
}
