package pafgames.stonebreaker.view.board;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import pafgames.stonebreaker.view.utility.ImageLoader;

/**
 * JPanel used as title panel.
 * It shows title and symbols.
 */
public class TitlePanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor of titlePanel.
     */
    public TitlePanel() {
        this.setOpaque(false);
        this.setLayout(new FlowLayout());
        this.add(ImageLoader.loadGif("/Images/diamond36.png"));
        this.add(ImageLoader.loadGif("/Images/diamond36S.png"));
        this.add(ImageLoader.loadGif("/Images/diamond36.png"));
    }
}
