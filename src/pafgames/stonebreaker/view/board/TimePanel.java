package pafgames.stonebreaker.view.board;

import javax.swing.JLabel;

/**
 * This class models the right panel of the main game view.
 * This panel contains a JLabel showing the actual
 * value of Time (or Moves) remained.
 *
 * @author Pucci
 */
public interface TimePanel {
    /**
     * This method is called by the BoardGUIImpl to
     * get the JLabel that show the actual value of Moves or Time.
     *
     * @return the JLabel used to show the actual
     * value of Moves or Time (remained).
     */
    JLabel getTimeLabel();

}
