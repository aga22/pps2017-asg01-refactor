package pafgames.stonebreaker.view.board;

import javax.swing.JButton;
import javax.swing.JWindow;

/**
 * This class models the GUI's bottom panel.
 */
public interface BottomPanel {

    /**
     * This method is called by the principal BoardGUIImpl
     * to have control of the Pause Window.
     *
     * @return the default window when the game is paused.
     */
    JWindow getPauseWindow();

    /**
     * This method is used to set the action
     * listener of the pause button by the BoardGUIImpl.
     *
     * @return the JButton which is used to set pause to the game.
     */
    JButton getTitleButton();

    /**
     * This method is used to set the action listener
     * of the exit button by the BoardGUIImpl.
     *
     * @return the JButton which is used to exit
     * from the game and return in the main menu.
     */
    JButton getMenuButton();

    /**
     * This method is used to set the action listener of
     * the resume button by the BoardGUIImpl.
     *
     * @return the JButton which is used to resume the game.
     */
    JButton getResumeButton();

}
