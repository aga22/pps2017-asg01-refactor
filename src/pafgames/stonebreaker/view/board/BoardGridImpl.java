package pafgames.stonebreaker.view.board;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import pafgames.stonebreaker.controller.game.Controller;
import pafgames.stonebreaker.model.Block;
import pafgames.stonebreaker.model.Gem;
import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.IconLoader;
import pafgames.stonebreaker.view.utility.StonesButton;

/**
 * This class implements the main Custom Panel with the Grid.
 * @author Pucci
 */
public class BoardGridImpl extends CJPanel implements BoardGrid {
        

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * This is the Controller instance present in the boardGrid.
	 */
	private Controller controller;

	/**
	 * This List of Custom buttons is used to show the actual
	 * Game situation.
	 * Every entity is seen as a custom JButton.
	 */
	private List<StonesButton> buttons;
      
	/**
	 * This is the constructor of the main game panel view.
	 * @param controller
	 *  is the controller instance passed by the parent JFrame.
	 */
	public BoardGridImpl(final Controller controller) {
        	super("BackGrid2.png");
                this.controller = controller;
		GridLayout experimentLayout = new GridLayout(
				this.controller.getRow(), this.controller.getColumn());
                this.setLayout(experimentLayout);
                this.updateGrid();
	}

	@Override
	public void updateGrid() {
		this.removeAll();
        this.buttons = new ArrayList<>();
        IntStream
        	.range(0, this.controller.getColumn() * this.controller.getRow())
        	.forEach(i -> {
	        	this.buttons.add(new StonesButton(i));
	            if (this.controller.getStoneByIndex(i).isPresent() && this.controller.getStoneByIndex(i).get() instanceof Block) {
	            	this.buttons.get(i).setIcon(IconLoader.STONE.getIcon());
	            } else {
	            	Gem gem = (Gem) this.controller.getStoneByIndex(i).get();
                    if (gem.getColour().equals(Gem.Type.PURPLE)) {
                        this.buttons.get(i).setIcon(IconLoader.PURPLE.getIcon());
                    } else
                	if (gem.getColour().equals(Gem.Type.WHITE)) {
                        this.buttons.get(i).setIcon(IconLoader.WHITE.getIcon());
                    } else
                    if (gem.getColour().equals(Gem.Type.YELLOW)) {
                            this.buttons.get(i).setIcon(IconLoader.YELLOW.getIcon());
                    } else
                    if (gem.getColour().equals(Gem.Type.RED)) {
                            this.buttons.get(i).setIcon(IconLoader.RED.getIcon());
                    } else
                    if (gem.getColour().equals(Gem.Type.CYAN)) {
                            this.buttons.get(i).setIcon(IconLoader.CYAN.getIcon());
                    } else
                    if (gem.getColour().equals(Gem.Type.GREEN)) {
                            this.buttons.get(i).setIcon(IconLoader.GREEN.getIcon());
                    }
	            }
	            this.buttons.get(i).addActionListener(this.controller.getStoneListener());
	            this.add(this.buttons.get(i));
        });
        this.updateUI();
	}

}