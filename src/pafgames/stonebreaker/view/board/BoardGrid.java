package pafgames.stonebreaker.view.board;

/**
 * This interface implements the main Custom Panel with the Grid.
 */
public interface BoardGrid {

    /**
     * this method is called by the principal BoardGUIImpl
     * to update the view of his game grid panel
     * it is successively called in the BoardGUIImpl from the Controller.
     */
    void updateGrid();
}
