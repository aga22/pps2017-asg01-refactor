package pafgames.stonebreaker.view.board;

import javax.swing.JButton;

/**
 * This class models the JPanel on the left of the main game view.
 *
 * @author Pucci
 */
public interface ScorePanel {

    /**
     * This method is called by the BoardGUIImpl and
     * recursively by the Controller to set the actual score.
     *
     * @param score -- the value to print on the JLabel
     *              used to show actual score.
     */
    void updateScore(final int score);

    /**
     * This method is called by the BoardGUIImpl
     * to get the JButton used for the PowerUp.
     *
     * @return the custom JButton that represent the powerUp *THOR*.
     */
    JButton getThor();

    /**
     * This method is called by the BoardGUIImpl to get the Score value.
     *
     * @return the actual Value on the JLabel used to show the actual Score.
     */
    String getScore();
}
