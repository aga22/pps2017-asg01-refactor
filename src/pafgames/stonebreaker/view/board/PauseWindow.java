package pafgames.stonebreaker.view.board;

import javax.swing.JButton;

/**
 * This class models the JWindow used to set pause to the game.
 *
 * @author Pucci
 */
public interface PauseWindow {

    /**
     * This method is used by the BoardGUIImpl to set the exit's action Listener.
     *
     * @return the JButton used to exit the game and return to the main menu.
     */
    JButton getMenuButton();

    /**
     * This method is used by the BoardGUIImpl to set the resume's action Listener.
     *
     * @return the JButton used to resume the game.
     */
    JButton getRewindButton();

}
