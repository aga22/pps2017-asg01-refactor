package pafgames.stonebreaker.view.board;

import java.awt.BorderLayout;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import pafgames.stonebreaker.controller.game.Controller;
import pafgames.stonebreaker.view.menu.GameOverFrame;
import pafgames.stonebreaker.view.menu.GameWindow;
import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * This class models the JFrame that contains all the Panels
 * used in the main game GUI.
 */
public class BoardGUIImpl extends JFrame implements BoardGUI {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * This is the instance of the Controller which
     * controls the game.
     */
    private final Controller controller;

    /**
     * This is the custom JPanel used to contains all the elements.
     */
    private final BoardPanel bp;

    /**
     * This is the constructor of the BoardGUIImpl JFrame.
     *
     * @param controller This is the instance of the Controller which controls the game.
     */
    public BoardGUIImpl(final Controller controller) {
        this.controller = controller;
        this.bp = new BoardPanel();
        this.setSize(Sizes.DEF_WIDTH, Sizes.DEF_HEIGHT);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.getContentPane().add(bp);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    @Override
    public void updateScore(final int score) {
        this.bp.sp.updateScore(score);
    }

    @Override
    public void updateView() {
        this.bp.expPanel.updateGrid();
    }

    @Override
    public void setTime(final int time) {
        try {
            SwingUtilities.invokeAndWait(() -> BoardGUIImpl.this.bp.timep.getTimeLabel().setText("  " + time));
        } catch (InvocationTargetException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setEnd(final List<Integer> listOfScores) {
        BoardGUIImpl.this.dispose();
        new GameOverFrame(BoardGUIImpl.this.bp.sp.getScore(), listOfScores);
    }

    @Override
    public void enablePowerUp() {
        this.bp.sp.getThor().setEnabled(true);
    }

    /**
     * This is the main game principal custom Panel.
     * This panel contains all the other sub-components.
     */
    private class BoardPanel extends CJPanel {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        /**
         * This is the instance of the main game boardGrid.
         */
        private final BoardGridImpl expPanel;
        /**
         * This is the instance of the main game TitlePanel.
         */
        private final TitlePanel tp = new TitlePanel();
        /**
         * This is the instance of the main game ScorePanelImpl.
         */
        private final ScorePanelImpl sp = new ScorePanelImpl(BoardGUIImpl.this.controller);
        /**
         * This is the instance of the main game TimePanelImpl.
         */
        private final TimePanelImpl timep = new TimePanelImpl();
        /**
         * This is the instance of the main game BottomPanelImpl.
         */
        private final BottomPanelImpl bp = new BottomPanelImpl();

        /**
         * This is the constructor of the main GUI principal panel.
         */
        public BoardPanel() {
            super("BackBlack.jpeg");
            this.expPanel = new BoardGridImpl(BoardGUIImpl.this.controller);
            this.setLayout(new BorderLayout());
            this.add(this.tp, BorderLayout.NORTH);
            this.add(this.sp, BorderLayout.WEST);
            this.add(this.expPanel, BorderLayout.CENTER);
            this.add(this.timep, BorderLayout.EAST);
            this.add(this.bp, BorderLayout.SOUTH);

            this.bp.getMenuButton().addActionListener(e -> {
                this.bp.getPauseWindow().dispose();
                new GameWindow();
                BoardGUIImpl.this.dispose();
            });
            this.bp.getTitleButton().addActionListener(e -> {
                this.bp.getTitleButton().setEnabled(!this.bp.getTitleButton().isEnabled());
                BoardGUIImpl.this.setEnabled(false);
                this.bp.getPauseWindow().setVisible(true);
            });
            this.bp.getTitleButton().addActionListener(BoardGUIImpl.this.controller.getTimeListener(true));
            this.bp.getResumeButton().addActionListener(e -> {
                this.bp.getTitleButton().setEnabled(!this.bp.getTitleButton().isEnabled());
                BoardGUIImpl.this.setEnabled(true);
                this.bp.getPauseWindow().dispose();
            });
            this.bp.getResumeButton().addActionListener(BoardGUIImpl.this.controller.getTimeListener(false));
        }
    }
}
