package pafgames.stonebreaker.view.board;

import java.util.List;

/**
 * Interface that models the inner Game view and
 * which contains all the components of it.
 * It's the only view interface seen by the controller.
 */
public interface BoardGUI {

    /**
     * This function is called by the controller to End the game.
     * It'll dispose the Game frame and set the GameOver frame visible.
     *
     * @param listOfScores List of saved highscores.
     */
    void setEnd(List<Integer> listOfScores);

    /**
     * This method is called by the controller
     * to set Enabled the PowerUp -- Thor -- button.
     */
    void enablePowerUp();

    /**
     * This method is called by the controller
     * to show on display Time (or Moves) remained.
     *
     * @param time Value to show on TimePanelImpl. (Moves or Time).
     */
    void setTime(final int time);

    /**
     * This method is called by the controller
     * to show on display the actual Score.
     *
     * @param score The valeu to show on scorePanel.
     */
    void updateScore(final int score);

    /**
     * This method is called by the controller to update the view.
     */
    void updateView();

}
