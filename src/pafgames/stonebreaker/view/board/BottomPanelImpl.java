package pafgames.stonebreaker.view.board;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JWindow;

import pafgames.stonebreaker.view.utility.GenericButton;
import pafgames.stonebreaker.view.utility.ImageLoader;

/**
 * This class models the bottom Panel of the main game GUI.
 */
public class BottomPanelImpl extends JPanel implements BottomPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This is the button used to set pause.
	 */
	private final GenericButton tmb = new GenericButton("Action");
	
	/**
	 * This is the instance of the window 
	 * visible when the pauseButton is pressed.
	 */
	private PauseWindowImpl pwindow = new PauseWindowImpl();
	
	/**
	 * Constructor of the BottomPanelImpl.
	 */
	public BottomPanelImpl() {
		this.setOpaque(false);
		this.pwindow.setVisible(false);
		this.setLayout(new BorderLayout());
		this.add(ImageLoader.loadGif("/Images/diamond.png"), BorderLayout.WEST);
		this.add(tmb, BorderLayout.EAST);
	}
	
	@Override
	public JWindow getPauseWindow() {
		return this.pwindow;
	}
	
	@Override
	public JButton getTitleButton() {
		return this.tmb;
	}
	
	@Override
	public JButton getMenuButton() {
		return this.pwindow.getMenuButton();
	}
	
	@Override
	public JButton getResumeButton() {
		return this.pwindow.getRewindButton();
	}
}
