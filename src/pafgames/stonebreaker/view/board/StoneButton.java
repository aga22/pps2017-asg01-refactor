package pafgames.stonebreaker.view.board;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JButton;

public class StoneButton extends JButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public StoneButton() {
        this.setBorderPainted(true);
        this.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.orange.darker().darker()));
        this.setContentAreaFilled(false);
    }
}
