package pafgames.stonebreaker.view.board;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import pafgames.stonebreaker.view.utility.ImageLoader;

/**
 * This is the JPanel used to show Time or moves remained.
 */
public class TimePanelImpl extends JPanel implements TimePanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Used as numbers to divide JPanel in 7 sectors.
     * Casual number to dispone time or moves not to distant
     * from his JLabel.
     */
    private static final int FRACT = 7;

    /**
     * JLabel used to show time or moves remained.
     */
    private JLabel timeLabel = new JLabel();

    /**
     * Constructor of TimePanelImpl.
     */
    public TimePanelImpl() {
        this.setOpaque(false);
        GridLayout vol = new GridLayout(FRACT, 0);
        this.setLayout(vol);
        this.add(ImageLoader.loadGif("/Images/clock.png"));
        final Font myFont = new Font("SansSerif", Font.BOLD, 52);
        this.timeLabel.setFont(myFont);
        this.add(this.timeLabel);
    }

    @Override
    public JLabel getTimeLabel() {
        return this.timeLabel;
    }
}
