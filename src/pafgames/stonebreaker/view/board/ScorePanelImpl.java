package pafgames.stonebreaker.view.board;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import pafgames.stonebreaker.controller.game.Controller;
import pafgames.stonebreaker.view.utility.GenericButton;
import pafgames.stonebreaker.view.utility.ImageLoader;

/**
 * This class models the Panel used to show Score
 * and that contains the PowerUp.
 */
public class ScorePanelImpl extends JPanel implements ScorePanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Int used as separator for elements in GridBagLayout.
	 */
	private static final int MINT = 20;
	/**
	 * Int used as separator for elements in GridBagLayout.
	 */
	private static final int MAXT = 100;
	/**
	 * This is the JLabel used to show score.
	 */
	private final JLabel scoreLabel = new JLabel();
	
	/**
	 * This is the Custom Button used for the 
	 * PowerUp.
	 */
	private final GenericButton thorButton = new GenericButton("Thor");
	
	/**
	 * This is the instance of the Controller.
	 */
	private final Controller controller;
	
	/**
	 * Constructor of ScorePanelImpl.
	 * @param controller
	 * 		The controller passed by the Game GUI.
	 */
	public ScorePanelImpl(final Controller controller) {
		this.controller = controller;
		this.setOpaque(false);
		this.setLayout(new GridBagLayout());
		GridBagConstraints grid = new GridBagConstraints();		
		grid.gridx = 0;
		grid.gridy = 0;
		this.add(ImageLoader.loadGif("/Images/Score.png"), grid);
		grid.ipady = MINT;
		grid.gridy = 1;
		final Font myFont = new Font("SansSerif", Font.BOLD, 36);
		this.scoreLabel.setForeground(Color.red.darker());
		this.scoreLabel.setFont(myFont);
		this.scoreLabel.setText("  " + 0);
		this.add(this.scoreLabel, grid);
		grid.ipady = MAXT;
		grid.gridy = 2;
		this.add(this.thorButton, grid);
		this.thorButton.setEnabled(false);
		this.thorButton.addActionListener(this.controller.getPowerUpListener());
		grid.ipady = 0;
	}
	
	@Override
	public void updateScore(final int score) {
		scoreLabel.setText("  " + score);
	}
	
	@Override
	public JButton getThor() {
		return this.thorButton;
	}
	
	@Override
	public String getScore() {
		return this.scoreLabel.getText();
	}
}
