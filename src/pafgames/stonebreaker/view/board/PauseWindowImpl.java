package pafgames.stonebreaker.view.board;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.geom.Ellipse2D;

import javax.swing.JButton;
import javax.swing.JWindow;

import pafgames.stonebreaker.view.utility.CJPanel;
import pafgames.stonebreaker.view.utility.GenericButton;
import pafgames.stonebreaker.view.utility.Sizes;

/**
 * 
 * This class models the JWindow visible when the button pause is pressed.
 */
public class PauseWindowImpl extends JWindow implements PauseWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Int used as a value for distancing elements
	 * in the GridBagLayout.
	 */
	private static final int DISTANCE = 50;

	/**
	 * This is the button used to go back to the main menu.
	 */
	private GenericButton menuButton = new GenericButton("ExitButton");
	
	/**
	 * This is the button used to resume the game.
	 */
	private GenericButton rewindButton = new GenericButton("BackButton");
	
	/**
	 * Constructor of the JWindow used for pause.
	 */
	public PauseWindowImpl() {
		
		this.setAlwaysOnTop(true);
		this.setSize(Sizes.PAUSE, Sizes.PAUSE);
		this.setShape(new Ellipse2D.Double(0, 0, getWidth(), getHeight()));
		this.add(new PausePanel());
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	/**
	 * Main panel of the JWindow used for pause.
	 */
	private class PausePanel extends CJPanel {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructor of PausePanel.
		 */
		public PausePanel() {
			super("BackBlack.jpeg");
			this.setLayout(new GridBagLayout());
			GridBagConstraints grid = new GridBagConstraints();
			grid.gridx = 0;
			grid.gridy = 0;
			this.add(PauseWindowImpl.this.rewindButton);
			grid.ipady = DISTANCE;
			grid.gridy = 1;
			this.add(PauseWindowImpl.this.menuButton);

		}
	}
	
	@Override
	public JButton getMenuButton() {
		return this.menuButton;
	}
	
	@Override
	public JButton getRewindButton() {
		return this.rewindButton;
	}
}
